import cv2
import sys
import numpy as np

def mse(a, b):
	err = np.sum((a-b) ** 2)
	err /= float(a.shape[0] * b.shape[1])
	return err


camera = cv2.imread('diff/c211.jpg',cv2.IMREAD_COLOR);
pallet = cv2.imread('diff/p211.jpg',cv2.IMREAD_COLOR);

camera = cv2.cvtColor(camera, cv2.COLOR_BGR2HSV)
pallet = cv2.cvtColor(pallet, cv2.COLOR_BGR2HSV)

# camera = cv2.medianBlur(camera,5)
# pallet = cv2.medianBlur(pallet,5)

camera = cv2.cvtColor(camera, cv2.COLOR_BGR2GRAY)
pallet = cv2.cvtColor(pallet, cv2.COLOR_BGR2GRAY)

# cameraHSV = cv2.cvtColor(cameraHSV, cv2.COLOR_BGR2GRAY)
# palletHSV = cv2.cvtColor(palletHSV, cv2.COLOR_BGR2GRAY)

# camera = camera[140:240, 70:170]
# pallet = pallet[140:240, 70:170]

# _, camera = cv2.threshold(camera, 127,255, cv2.THRESH_BINARY)
# _, pallet = cv2.threshold(pallet, 127,255, cv2.THRESH_BINARY)

imgBW = abs(pallet - camera)
hhsize = np.sum(imgBW != 0)

cv2.imwrite('diff/c211.jpg', camera)
cv2.imwrite('diff/p211.jpg', pallet)
cv2.imwrite('diff/o211.jpg', imgBW)

h, w = imgBW.shape
p = (hhsize / (h * w)) * 100
e = (mse(pallet, camera) + mse(camera, pallet)) / 2
# print((p + e) / 2)
# print(p)
print(int(e))
sys.stdout.flush()
