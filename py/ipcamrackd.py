import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests
from utils import WebcamVideoStream

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

# stream = cv2.VideoCapture('rtsp://192.168.108.40:554/ucast/11')
# stream = cv2.VideoCapture('rtsp://192.168.108.156:554/11')
stream = WebcamVideoStream(src='rtsp://192.168.108.161:554/11', width=1920, height=1080).start()
later = time.time()

while True:
	# (grabbed, frame) = stream.read()
	frame = stream.read()
	fh, fw, _ = frame.shape
	
	# print(fh, fw)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	frame = cv2.blur(frame,(4,4))

	c0 = frame[620:780, 240:390]
	c1 = frame[640:780, 540:700]
	e0 = find17(c0)
	e1 = find17(c1)

	out = -1
	if e0:
		out = (0)
	elif e1:
		out = (1)
     
	now = time.time()
	difference = (now - later)
	# print(difference)
	if difference >= 0.8:
		# cv2.imwrite("w.png", frame);
		later = now
		# print(later)
		print(e0, e1, str(out))
		try:
			r = requests.get('http://192.168.108.7:3310/buffer/setrackd/'+ str(out))
			# print(r)
		except:
			# print('err')
			_ = 1

	cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	aruco.drawDetectedMarkers(frame, cc, ii)

	# frame = cv2.resize(frame, (int(fw/2), int(fh/2)))
	# cv2.imshow('frame',frame)
	# cv2.imshow('0',c0)
	# cv2.imshow('1',c1)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()