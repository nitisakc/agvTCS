import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

stream = cv2.VideoCapture('http://192.168.101.119:8081/')

while True:
	(grabbed, frame) = stream.read()
	fh, fw, _ = frame.shape

	cv2.rectangle(frame, (560, 160), (568, 166), (0,0,0), 3)
	cv2.rectangle(frame, (797, 546), (831, 551), (10,10,10), 5)
	cv2.rectangle(frame, (797, 552), (808, 560), (10,10,10), 5)
	cv2.rectangle(frame, (568, 565), (570, 575), (0,0,0), 3)
	cv2.rectangle(frame, (554, 563), (562, 572), (160,160,160), 3)

	cv2.rectangle(frame, (240, 167), (300, 168), (200,200,200), 3)
	
	print(fh, fw)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	c7  = frame[90:220, 200:350]

	frame = cv2.blur(frame,(4,4))

	c14 = frame[90:220, 750:880]
	c10 = frame[90:220, 470:620]
	c56 = frame[520:640, 750:880]
	c57 = frame[520:640, 470:620]
	c58 = frame[520:640, 200:350]

	(_, c7) = cv2.threshold(c7, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	(_, c14) = cv2.threshold(c14, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	# (_, c56) = cv2.threshold(c56, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	(_, c57) = cv2.threshold(c57, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	e14 = find17(c14)
	e10 = find17(c10)
	e7  = find17(c7)
	e56 = find17(c56)
	e57 = find17(c57)
	e58 = find17(c58)
    
	e = [e56,e57,e58,e14,e10,e7]
	out = ','.join([str(x) for x in e])
	print(out)
	requests.get('http://localhost:3320/buffer/wipout/set/'+ str(out))

	# cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, cc, ii)

	# cv2.imshow('frame',frame)
	time.sleep(2)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()