import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 10 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

camera1 = cv2.imread('camera/211.jpg', 0);
camera2 = cv2.imread('camera/212.jpg', 0);

cv2.bilateralFilter(camera1,9,75,75)
cv2.bilateralFilter(camera2,9,75,75)

c14 = camera1[150:230, 170:260]
c10 = camera1[20:110, 180:280]
c7 = camera2[10:70, 20:100]
c56 = camera1[140:220, 0:80]
c57 = camera1[10:100, 0:80]
c58 = camera2[20:90, 230:310]
c69 = camera2[160:240, 220:310]

e14 = find17(c14)
e10 = find17(c10)
e7  = find17(c7)
e56 = find17(c56)
e57 = find17(c57)
e58 = find17(c58)
e69 = find17(c69)

out = 0
# if e7:
# 	out = (7)

# if e10 and e7:
# 	out = (10)

# if e14 and e10 and e7:
# 	out = (14)

if e58 #and e14 and e10 and e7:
	out = (58)

if e57 and e58 #and e14 and e10 and e7:
	out = (57)

if e56 and e57 and e58 #and e14 and e10 and e7:
	out = (56)

print(out, e56, e57, e58, e14, e10, e7)
sys.stdout.flush()
