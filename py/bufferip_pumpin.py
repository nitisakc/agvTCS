import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

stream = cv2.VideoCapture('rtsp://192.168.108.131:554/11')
later = time.time()

while True:
	(grabbed, frame) = stream.read()
	fh, fw, _ = frame.shape
	
	# print(fh, fw)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	frame = cv2.blur(frame,(4,4))

	# c0 = frame[682:855, 1660:1823]
	# c1 = frame[714:885, 1425:1582]
	# c2 = frame[734:912, 1007:1177]
	# c3 = frame[724:905, 531:706]
	# c4 = frame[722:892, 253:414]

	c0 = frame[652:825, 1660:1843]
	c1 = frame[684:855, 1425:1602]
	c2 = frame[704:882, 1007:1197]
	c3 = frame[694:875, 531:746]
	c4 = frame[692:862, 253:434]

	c5 = frame[182:325, 1660:1843]
	c6 = frame[154:305, 1425:1602]
	c7 = frame[104:302, 1007:1197]
	c8 = frame[124:295, 580:790]
	c9 = frame[152:302, 303:485]


	# cv2.rectangle(c1, (136, 37), (139, 43), (30,30,30), -1)
	(_, c1) = cv2.threshold(c1, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	cv2.rectangle(c3, (158, 25), (162, 43), (30,30,30), -1)
	# (_, c3) = cv2.threshold(c3, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    # //3142

	# c7 = cv2.cvtColor(c7, cv2.COLOR_BGR2GRAY)
	# (_, c14) = cv2.threshold(c14, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	# (_, c56) = cv2.threshold(c56, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	# (_, c57) = cv2.threshold(c57, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	e0 = find17(c0)
	e1 = find17(c1)
	e2 = find17(c2)
	e3 = find17(c3)
	e4 = find17(c4)
	e5 = find17(c5)
	e6 = find17(c6)
	e7 = find17(c7)
	e8 = find17(c8)
	e9 = find17(c9)

	out = -1
	if e4:
		# if e9:
		# 	out = (9)
		if e8 and e9:
			out = (8)
		if e7 and e8 and e9:
			out = (7)
		if e6 and e7 and e8 and e9:
			out = (6)
		if e5 and e6 and e7 and e8 and e9:
			out = (5)
	#
	if out == -1:
		if e4:
			out = (4)
		if e3 and e4:
			out = (3)
		if e2 and e3 and e4:
			out = (2)
		if e1 and e2 and e3 and e4:
			out = (1)
		if e0 and e1 and e2 and e3 and e4:
			out = (0)

	now = time.time()
	difference = (now - later)
	# print(difference)
	if difference >= 0.8:
		# cv2.imwrite("w.png", frame);
		later = now
		# print(later)
		print(e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, str(out))
		# requests.get('http://localhost:3310/buffer/set/'+ str(out))

	cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	aruco.drawDetectedMarkers(frame, cc, ii)

	# cv2.imshow('0',c0)
	# cv2.imshow('c8',c8)
	# cv2.imshow('c9',c9)
	# cv2.imshow('3',c3)
	# cv2.imshow('2',c2)
	# cv2.imshow('3',c3)
	# cv2.imshow('4',c4)
	frame = cv2.resize(frame, (int(fw/2), int(fh/2)))
	cv2.imshow('frame',frame)
	time.sleep(0.5)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()