import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests
from utils import WebcamVideoStream

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

stream = WebcamVideoStream(src='rtsp://192.168.108.131:554/11', width=1920, height=1080).start()
later = time.time()

while True:
	# (grabbed, frame) = stream.read()
	frame = stream.read()
	fh, fw, _ = frame.shape
	
	# print(fh, fw)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	frame = cv2.blur(frame,(4,4))

	c0 = frame[750:900, 1530:1670]
	c1 = frame[400:560, 1530:1670]
	c2 = frame[790:930, 1200:1350]
	c3 = frame[420:580, 1200:1340]
	c4 = frame[800:950, 840:1000]
	c5 = frame[440:600, 810:980]
 
	e0 = find17(c0)
	e1 = find17(c1)
	e2 = find17(c2)
	e3 = find17(c3)
	e4 = find17(c4)
	e5 = find17(c5)

	out = -1
	if e1:
		out = (1)
	if e0 and e1:
		out = (0)
  
	if out == -1:
		if e3:
			out = (3)
		if e2 and e3:
			out = (2)
	
	if out == -1:
		if e5:
			out = (5)
		if e4 and e5:
			out = (4)
     


	now = time.time()
	difference = (now - later)
	# print(difference)
	if difference >= 0.8:
		# cv2.imwrite("w.png", frame);
		later = now
		# print(later)
		print(e0, e1, e2, e3, e4, e5, str(out))
		try:
			# _ = 1
			requests.get('http://localhost:3310/buffer/setother/'+ str(out))
		except:
			_ = 1

	# cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, cc, ii)

	# frame = cv2.resize(frame, (int(fw/2), int(fh/2)))
	# cv2.imshow('frame',frame)
	# cv2.imshow('0',c0)
	# cv2.imshow('1',c1)
	# cv2.imshow('2',c2)
	# cv2.imshow('3',c3)
	# cv2.imshow('4',c4)
	# cv2.imshow('5',c5)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()