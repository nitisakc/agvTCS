import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time

def mse(a, b):
	err = np.sum((a-b) ** 2)
	err /= float(a.shape[0] * b.shape[1])
	return err

# while True:
camera1 = cv2.imread('camera/211.jpg', 0);
pallet1 = cv2.imread('pallet/211.jpg', 0);
camera2 = cv2.imread('camera/212.jpg', 0);
pallet2 = cv2.imread('pallet/212.jpg', 0);

# camera = cv2.cvtColor(camera, cv2.COLOR_BGR2HSV)
# pallet = cv2.cvtColor(pallet, cv2.COLOR_BGR2HSV)

# camera = cv2.medianBlur(camera,5)
# pallet = cv2.medianBlur(pallet,5)

# camera = cv2.cvtColor(camera, cv2.COLOR_BGR2GRAY)
# pallet = cv2.cvtColor(pallet, cv2.COLOR_BGR2GRAY)

c14 = camera1[150:210, 120:210]
p14 = pallet1[150:210, 120:210]

c10 = camera1[0:60, 120:210]
p10 = pallet1[0:60, 120:210]

c7 = camera2[100:150, 90:180]
p7 = pallet2[100:150, 90:180]

c56 = camera1[150:210, 0:90]
p56 = pallet1[150:210, 0:90]	

c57 = camera1[70:120, 0:90]
p57 = pallet1[70:120, 0:90]

c58 = camera2[50:100, 220:320]
p58 = pallet2[50:100, 220:320]


e14 = int((mse(p14, c14) + mse(c14, p14)) / 2)
e10 = int((mse(p10, c10) + mse(c10, p10)) / 2)
e7  = int((mse(p7, c7) + mse(c7, p7)) / 2)
e56 = int((mse(p56, c56) + mse(c56, p56)) / 2)
e57 = int((mse(p57, c57) + mse(c57, p57)) / 2)
e58 = int((mse(p58, c58) + mse(c58, p58)) / 2)

out = 0
vl = 50
if e7 < vl:
	out = (7)

if e10 < vl and e7 < vl:
	out = (10)

if e14 < vl and e10 < vl and e7 < vl:
	out = (14)

# if e58 < vl and e14 < vl and e10 < vl and e7 < vl:
# 	out = (58)

# if e57 < vl and e58 < vl and e14 < vl and e10 < vl and e7 < vl:
# 	out = (57)

# if e56 < vl and e57 < vl and e58 < vl and e14 < vl and e10 < vl and e7 < vl:
# 	out = (56)

print(e56, e57, e58, e14, e10, e7, 0)
	# requests.get('http://192.168.101.43:3001/to/setbuf/'+ str(out))

	# numpy_vertical_concat = np.concatenate((c7, c10), axis=0)
	# cv2.imshow('numpy_vertical_concat',numpy_vertical_concat)
	# cv2.imshow('c7',c7)
	# cv2.imshow('c10',c10)
	# cv2.imshow('c14',c14)
	# cv2.imshow('c58',c58)
	# cv2.imshow('c57',c57)
	# cv2.imshow('c56',c56)

	# time.sleep(5)
	# if cv2.waitKey(1) & 0xFF == ord('q'):
	# 	break

# _, camera = cv2.threshold(camera, 127,255, cv2.THRESH_BINARY)
# _, pallet = cv2.threshold(pallet, 127,255, cv2.THRESH_BINARY)

# imgBW = abs(pallet - camera)
# hhsize = np.sum(imgBW != 0)

# # cv2.imwrite('diff/c212-0.jpg', camera)
# # cv2.imwrite('diff/p212-0.jpg', pallet)
# # cv2.imwrite('diff/o212-0.jpg', imgBW)


# h, w = imgBW.shape
# p = (hhsize / (h * w)) * 100
# e = (mse(pallet, camera) + mse(camera, pallet)) / 2
# # print((p + e) / 2)
# # print(p)
# print(int(e))
# while True:
# 	cv2.imshow('c1',c1)
# 	cv2.imshow('c2',c2)
# 	cv2.imshow('c3',c3)

# 	if cv2.waitKey(1) & 0xFF == ord('q'):
# 		break

# cv2.destroyAllWindows()
sys.stdout.flush()
