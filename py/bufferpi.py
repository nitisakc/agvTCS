import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

stream = cv2.VideoCapture('http://192.168.101.119:8081/')

while True:
	(grabbed, frame) = stream.read()
	fh, fw, _ = frame.shape

	# cv2.rectangle(frame, (280, 140), (283, 152), (0,0,0), 2)
	# cv2.rectangle(frame, (797, 546), (831, 551), (0,0,0), 5)
	# cv2.rectangle(frame, (797, 552), (808, 560), (0,0,0), 5)
	
	# cv2.rectangle(frame, (533, 552), (566, 558), (0,0,0), 7)
	# cv2.rectangle(frame, (533, 554), (543, 580), (0,0,0), 7)
	# cv2.rectangle(frame, (566, 565), (569, 575), (0,0,0), 4)

	cv2.rectangle(frame, (560, 160), (568, 166), (0,0,0), 3)
	cv2.rectangle(frame, (797, 546), (831, 551), (10,10,10), 5)
	cv2.rectangle(frame, (797, 552), (808, 560), (10,10,10), 5)
	cv2.rectangle(frame, (568, 565), (570, 575), (0,0,0), 3)
	cv2.rectangle(frame, (554, 563), (562, 572), (160,160,160), 3)
	
	print(fh, fw)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	frame = cv2.blur(frame,(4,4))
	# cv2.imshow('c7',frame)

	c14 = frame[90:220, 750:880]
	c10 = frame[90:220, 470:620]
	c7  = frame[90:220, 200:350]
	c56 = frame[520:640, 750:880]
	c57 = frame[520:640, 470:620]
	c58 = frame[520:640, 200:350]
	# c14 = frame[410:510, 190:290]
	# c10 = frame[410:510, 340:440]
	# c7  = frame[410:510, 500:600]
	# c56 = frame[160:260, 190:290]
	# c57 = frame[150:250, 340:440]
	# c58 = frame[150:250, 500:600]
	# c69 = frame[160:240, 220:310]
	(_, c7) = cv2.threshold(c7, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	(_, c14) = cv2.threshold(c14, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	(_, c56) = cv2.threshold(c56, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
	(_, c57) = cv2.threshold(c57, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	e14 = find17(c14)
	e10 = find17(c10)
	e7  = find17(c7)
	e56 = find17(c56)
	e57 = find17(c57)
	e58 = find17(c58)
	# e69 = find17(c69)


	out = 0
	if e7:
		out = (7)

	if e10 and e7:
		out = (10)

	if e14 and e10 and e7:
		out = (14)

	if e58:
		out = (58)

	if e57 and e58:
		out = (57)

	if e56 and e57 and e58:
		out = (56)

	# if e7:
	# 	out = (7)

	# if e10 and e7:
	# 	out = (10)

	# # if e10 and e7:
	# # 	out = (14)

	# if e58 and e10 and e7:
	# 	out = (58)

	# if e57 and e58 and e10 and e7:
	# 	out = (57)

	# if e56 and e57 and e58 and e10 and e7:
	# 	out = (56)

	# if e10:
	# 	out = (10)

	# if e14 and e10:
	# 	out = (14)

	# if e58 and e14 and e10:
	# 	out = (58)

	# if e57 and e58 and e14 and e10:
	# 	out = (57)

	# if e56 and e57 and e58 and e14 and e10:
	# 	out = (56)

	# if e7:
	# 	out = (7)

	# if e10 and e7:
	# 	out = (10)

	# if e14 and e10 and e7:
	# 	out = (14)

	# if e58 and e14 and e10 and e7:
	# 	out = (58)

	# if e57 and e58 and e14 and e10 and e7:
	# 	out = (57)

	# if e56 and e57 and e58 and e14 and e10 and e7:
	# 	out = (56)

	print(e56, e57, e58, e14, e10, e7, out)
	requests.get('http://localhost:3310/buffer/set/'+ str(out))

	# cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, cc, ii)

	# # cv2.imshow('c69',c69)
	# cv2.imshow('c14',c14)
	# cv2.imshow('c10',c10)
	# cv2.imshow('c7',c7)
	# cv2.imshow('frame',frame)
	time.sleep(0.5)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()
