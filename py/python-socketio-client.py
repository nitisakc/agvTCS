# pip install socketIO-client3
from socketio_client import SocketIO, LoggingNamespace
import time

def on_connect():
    print('connect')

def on_disconnect():
    print('disconnect')

def on_reconnect():
    print('reconnect')

def on_aaa_response(*args):
    print('on_aaa_response', args)

def on_message(data):
	print('message received with ', data)

socketIO = SocketIO('localhost', 3310, LoggingNamespace)
socketIO.on('connect', on_connect)
socketIO.on('disconnect', on_disconnect)
socketIO.on('reconnect', on_reconnect)
socketIO.on('pys', on_message)

while(True):
	val = int(time.time())
	print(val)
	socketIO.emit('py', {'response': val })
	# time.sleep(0.1)
	# socketIO.wait(seconds=0.01)