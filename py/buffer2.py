import cv2
import sys
import numpy as np
# import requests
import time

def mse(a, b):
	err = np.sum((a-b) ** 2)
	err /= float(a.shape[0] * b.shape[1])
	return err

while True:
	camera = cv2.imread('../camera/212.jpg', 0);
	pallet = cv2.imread('../pallet/212.jpg', 0);

# camera = cv2.cvtColor(camera, cv2.COLOR_BGR2HSV)
# pallet = cv2.cvtColor(pallet, cv2.COLOR_BGR2HSV)

# camera = cv2.medianBlur(camera,5)
# pallet = cv2.medianBlur(pallet,5)

# camera = cv2.cvtColor(camera, cv2.COLOR_BGR2GRAY)
# pallet = cv2.cvtColor(pallet, cv2.COLOR_BGR2GRAY)

	c1 = camera[200:240, 140:220]
	p1 = pallet[200:240, 140:220]

	c2 = camera[120:160, 140:220]
	p2 = pallet[120:160, 140:220]

	c3 = camera[20:60, 140:220]
	p3 = pallet[20:60, 140:220]


	e1 = (mse(p1, c1) + mse(c1, p1)) / 2
	e2 = (mse(p2, c2) + mse(c2, p2)) / 2
	e3 = (mse(p3, c3) + mse(c3, p3)) / 2

	out = 0
	if e3 < 35:
		out = (7)

	if e2 < 35 and e3 < 35:
		out = (10)

	if e1 < 35 and e2 < 35 and e3 < 35:
		out = (14)

	print(e1, e2, e3, out)
# requests.get('http://192.168.101.43:3001/to/setbuf/'+ str(out))
# cv2.imshow('c1',c1)
# cv2.imshow('c2',c2)
# cv2.imshow('c3',c3)

	# time.sleep(5)
	# if cv2.waitKey(1) & 0xFF == ord('q'):
	# 	break

# _, camera = cv2.threshold(camera, 127,255, cv2.THRESH_BINARY)
# _, pallet = cv2.threshold(pallet, 127,255, cv2.THRESH_BINARY)

# imgBW = abs(pallet - camera)
# hhsize = np.sum(imgBW != 0)

# # cv2.imwrite('diff/c212-0.jpg', camera)
# # cv2.imwrite('diff/p212-0.jpg', pallet)
# # cv2.imwrite('diff/o212-0.jpg', imgBW)


# h, w = imgBW.shape
# p = (hhsize / (h * w)) * 100
# e = (mse(pallet, camera) + mse(camera, pallet)) / 2
# # print((p + e) / 2)
# # print(p)
# print(int(e))
# while True:
	cv2.imshow('c1',abs(p1 - c1))
	cv2.imshow('c2',abs(p2 - c2))
	cv2.imshow('c3',abs(p2 - c3))

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()
#sys.stdout.flush()
