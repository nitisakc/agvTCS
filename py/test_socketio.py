import socketio
import time
# print(socketio)

sio = socketio.Client(reconnection=True, reconnection_attempts=1, reconnection_delay=5, reconnection_delay_max=20)

@sio.on('connect')
def on_connect():
	print('connection established')

@sio.on('pys')
def on_message(data):
	print('message received with ', data)
    # sio.emit('my response', {'response': 'my response'})

@sio.on('disconnect')
def on_disconnect():
	print('disconnected from server')

def my_background_task(my_argument):
	while(True):
		val = int(time.time())
		print(val)
		sio.emit('py', {'response': val })
		time.sleep(1)
	pass

sio.connect('http://localhost:3310')
sio.start_background_task(my_background_task, 123)
sio.wait()




