import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

stream = cv2.VideoCapture('rtsp://192.168.101.103:554/ucast/11')
later = time.time()
out = -1

while True:
	(grabbed, frame) = stream.read()
	fh, fw, _ = frame.shape
	
	# print(fh, fw)
	# frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	# (_, frame) = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	frame = cv2.blur(frame,(5,5))
	# cv2.imshow('c7',frame)

	c0 = frame[719:894, 358:533]
	c1 = frame[709:875, 670:851]
	c2 = frame[679:842, 1155:1326]
	c3 = frame[654:816, 1484:1640]

    # //3142

	e0 = find17(c0)
	e1 = find17(c1)
	e2 = find17(c2)
	e3 = find17(c3)

	if e3:
		out = (3)

	if e2 and e3:
		out = (2)

	if e1 and e2 and e3:
		out = (1)

	if e0 and e1 and e2 and e3:
		out = (0)

	now = time.time()
	difference = (now - later)
	# print(difference)
	if difference >= 0.4:
		# cv2.imwrite("w.png", frame);
		later = now
		# print(later)
		e = [e0, e1, e2, e3]
		arr = ','.join([str(x) for x in e])
		print(arr)
		print(out)
		requests.get('http://localhost:3310/buffer/setpump/'+ str(arr)+'/'+str(out))

	cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	aruco.drawDetectedMarkers(frame, cc, ii)

	# cv2.imshow('57',c57)
	# cv2.imshow('7',c7)
	# cv2.imshow('c10',c10)
	# cv2.imshow('c7',c7)
	# frame = cv2.resize(frame, (int(fw/2), int(fh/2)))
	# cv2.imshow('frame',frame)
	# time.sleep(0.5)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()