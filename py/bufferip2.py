import cv2
import sys
import numpy as np
import cv2.aruco as aruco
import time
import requests

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
parameters =  aruco.DetectorParameters_create()

def PolygonArea(c):
	c = c[0]
	c = [(c[0][0], c[0][1]), (c[1][0], c[1][1]), (c[2][0], c[2][1]), (c[3][0], c[3][1])]
	n = len(c) 
	area = 0.0
	for i in range(n):
		j = (i + 1) % n
		area += c[i][0] * c[j][1]
		area -= c[j][0] * c[i][1]
		area = abs(area) / 2.0
	return area

def find17(g):
	corners, ids, _ = aruco.detectMarkers(g, aruco_dict, parameters=parameters)
	# aruco.drawDetectedMarkers(frame, corners, ids)
	i = 0
	objs = []
	res = False
	for corner in corners:
		area = PolygonArea(corner)
		if area > 5 and int(ids[i]) == 17:
			res = True
		i = i + 1

	return res

stream = cv2.VideoCapture('rtsp://192.168.101.102:554/ucast/11')
later = time.time()

while True:
	(grabbed, frame) = stream.read()
	fh, fw, _ = frame.shape
	
	# print(fh, fw)
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	# frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	frame = cv2.blur(frame,(10,10))
	# cv2.imshow('c7',frame)

	# c56 = frame[163:319, 161:322]
	# c57 = frame[127:300, 517:692]
	# c58 = frame[120:302, 950:1148]
	
	c14 = frame[719:856, 188:317]
	c10 = frame[692:819, 458:599]
	c7 = frame[619:749, 911:1057]
	c13 = frame[537:692, 1294:1450]

	# (_, c7) = cv2.threshold(c7, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

	e56 = False#find17(c56)
	e57 = False#find17(c57)
	e58 = False#find17(c58)
	e14 = find17(c14)
	e10 = find17(c10)
	e7 = find17(c7)
	e13  = find17(c13)

	now = time.time()
	difference = (now - later)
	# print(difference)
	if difference >= 1.0:
		e = [e56,e57,e58,e14,e10,e7,e13]
		out = ','.join([str(x) for x in e])
		print(out)
		# cv2.imwrite("w.png", frame);
		later = now
		# print(later)
		# print(e56, e57, e58, e14, e10, e7, out)
		requests.get('http://localhost:3310/buffer/wipout/set/'+ str(out))

	cc, ii, _ = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
	aruco.drawDetectedMarkers(frame, cc, ii)

	# cv2.imshow('99',c99)
	# cv2.imshow('c7',c7)
	# cv2.imshow('c10',c10)
	# cv2.imshow('c14',c14)
	# frame = cv2.resize(frame, (int(fw/2), int(fh/2)))
	# cv2.imshow('frame',frame)
	# time.sleep(0.5)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cv2.destroyAllWindows()