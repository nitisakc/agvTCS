var express = require('express');
let mgdb = require('../utils/mgdb');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendfile('www/orders.html');
});

router.get('/history', function(req, res, next) {
  res.sendfile('www/history.html');
});

router.get('/logs', function(req, res, next) {
	mgdb.find('gridtcs', 'orders', { }, (err, doc)=>{
  		res.send(doc);
	});
});

router.get('/export', function(req, res, next) {
	mgdb.find('gridtcs', 'orders', {
    "dt" : {

            "$gte": new Date("2020-11-30T00:00:00.000Z")
        }
}, (err, doc)=>{
  		res.send(doc);
	});
});

module.exports = router;
