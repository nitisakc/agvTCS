var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendfile('www/cars.html');
});

router.get('/data', function(req, res, next) {
  res.send(global.cars);
});

router.get('/remote/:num', function(req, res, next) {
  res.sendfile(`www/remote.html`, { num: req.params.num });
  // res.redirect(`/www/remote.html`)
});

module.exports = router;
