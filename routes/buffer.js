const express = require('express');
const schedule = require('node-schedule');
const request = require('request');
const router = express.Router();

let reset = 0, resetpump = 0, resetrackd = 0, resetlift = 0, resetother = 0;
/* GET home page. */
router.get('/set/:id', function(req, res, next) {
	reset = 0;
	global.buffers.pimp.shift();
	global.buffers.pimp.push(req.params.id);

	// console.log('Set: ' + req.params.id);
	// console.dir(global.buffers);
	res.send(global.buffers.pimp);

	if(global.buffers.pimp && global.buffers.pimp.length >= 3){
		let s = -1;
		if(global.buffers.pimp[0] == global.buffers.pimp[1] && global.buffers.pimp[0] == global.buffers.pimp[2] && global.buffers.pimp[0] == global.buffers.pimp[3]){
			s = global.buffers.pimp[0];
		}
		global.buffer.wipin = s;
		// if(global.waitjob.pimp){
		// 	request.get(
	    //         `http://192.168.108.${global.waitjob.pimp}:3001/to/setbuf/${s}`,
	    //         (err, res, body)=>{
	    //             if(err){ console.error('error setbuf:', err);}
	    //             else{
	    //                 // console.log('Send Buffer ' + s);
	    //             }
	    //         }
	    //     );
	    // }
	}
});

router.get('/setpumpin/:id', function(req, res, next) {
	resetpump = 0;
	global.buffers.pump.shift();
	global.buffers.pump.push(req.params.id);

	// console.log('Set: ' + req.params.id);
	// console.dir(global.buffers);
	res.send(global.buffers.pump);

	if(global.buffers.pump && global.buffers.pump.length >= 3){
		let s = -1;
		if(global.buffers.pump[0] == global.buffers.pump[1] && global.buffers.pump[0] == global.buffers.pump[2] && global.buffers.pump[0] == global.buffers.pump[3]){
			s = global.buffers.pump[0];
		}
		global.buffer.pumpin = s;
	}
});

router.get('/setpump/:id/:num', function(req, res, next) {
	resetpump = 0;
	global.buffers.pump.shift();
	global.buffers.pump.push(req.params.num);
	// let b = req.params.id.split(',');
	// if(b[0] == 'False'){ global.buffers.jobs[0]; }

	// console.log('Set: ' + req.params.id);
	// console.dir(req.params.num);
	res.send(global.buffers.pump);

	if(global.buffers.pump && global.buffers.pump.length >= 3){
		let s = -1;
		if(global.buffers.pump[0] == global.buffers.pump[1] && global.buffers.pump[0] == global.buffers.pump[2] && global.buffers.pump[0] == global.buffers.pump[3]){
			s = global.buffers.pump[0];
		}
		if(global.waitjob.pump){
			request.get(
	            `http://192.168.108.${global.waitjob.pump}:3001/to/setbuf/${s}`,
	            (err, res, body)=>{
	                if(err){ console.error('error setbuf:', err);}
	                else{
	                    // console.log('Send Buffer ' + s);
	                }
	            }
	        );
	    }
	}
});

router.get('/setother/:id', function(req, res, next) {
	resetother = 0;
	global.buffers.other.shift();
	global.buffers.other.push(req.params.id);
	res.send(global.buffers.other);

	if(global.buffers.other && global.buffers.other.length >= 3){
		let s = -1;
		if(global.buffers.other[0] == global.buffers.other[1] && global.buffers.other[0] == global.buffers.other[2] && global.buffers.other[0] == global.buffers.other[3]){
			s = global.buffers.other[0];
		}
		global.buffer.other = s;
	}
});

router.get('/setrackd/:id', function(req, res, next) {
	resetrackd = 0;
	global.buffers.rackd.shift();
	global.buffers.rackd.push(req.params.id);

	// console.log('Set: ' + req.params.id);
	// console.dir(global.buffers);
	res.send(global.buffers.rackd);

	if(global.buffers.rackd && global.buffers.rackd.length >= 3){
		let s = -1;
		if(global.buffers.rackd[0] == global.buffers.rackd[1] && global.buffers.rackd[0] == global.buffers.rackd[2] && global.buffers.rackd[0] == global.buffers.rackd[3]){
			s = global.buffers.rackd[0];
		}
		global.buffer.rackd = s;
	}
});

router.get('/setlift/:id', function(req, res, next) {
	resetlift = 0;
	global.buffers.lift.shift();
	global.buffers.lift.push(req.params.id);

	// console.log('Set: ' + req.params.id);
	// console.dir(global.buffers);
	res.send(global.buffers.lift);

	if(global.buffers.lift && global.buffers.lift.length >= 3){
		let s = -1;
		if(global.buffers.lift[0] == global.buffers.lift[1] && global.buffers.lift[0] == global.buffers.lift[2] && global.buffers.lift[0] == global.buffers.lift[3]){
			s = global.buffers.lift[0];
		}
		global.buffer.lift = s;
	}
});

router.get('/wipout', function(req, res, next) {
	res.sendfile('www/wipout.html');
});

router.get('/wipout/data', function(req, res, next) {
	res.send(global.buffout.wip);
});

router.get('/wipout/set/:data', function(req, res, next) {
	global.buffout.wip.bool = req.params.data.split(',').map(function (x) { return x == 'True' });
	// console.log(global.buffout.wip.bool);
	if(global.buffout.wip.bool[0]){ global.buffout.wip.jobs[0] = null; }
	if(global.buffout.wip.bool[1]){ global.buffout.wip.jobs[1] = null; }
	if(global.buffout.wip.bool[2]){ global.buffout.wip.jobs[2] = null; }
	if(global.buffout.wip.bool[3]){ global.buffout.wip.jobs[3] = null; }
	if(global.buffout.wip.bool[4]){ global.buffout.wip.jobs[4] = null; }
	if(global.buffout.wip.bool[5]){ global.buffout.wip.jobs[5] = null; }
	if(global.buffout.wip.bool[6]){ global.buffout.wip.jobs[6] = null; }
	res.send(global.buffout.wip);
});

router.get('/wipout/setjob/:id/:target', function(req, res, next) {
	global.buffout.wip.jobs[req.params.id] = req.params.target == 'null' ? 0 : parseInt(req.params.target) ;
	res.send(global.buffout.wip);
});

schedule.scheduleJob('*/2 * * * * *', function(){
	reset++;
	if(reset > 3){
		global.buffers.pimp.shift();
		global.buffers.pimp.push(0);
	}

	resetpump++;
	if(resetpump > 3){
		global.buffers.pump.shift();
		global.buffers.pump.push(0);
	}
	resetrackd++;
	if(resetrackd > 3){
		global.buffers.rackd.shift();
		global.buffers.rackd.push(0);
	}
	resetlift++;
	if(resetlift > 3){
		global.buffers.lift.shift();
		global.buffers.lift.push(0);
	}
	resetother++;
	if(resetother > 3){
		global.buffers.other.shift();
		global.buffers.other.push(0);
	}
});

module.exports = router;
