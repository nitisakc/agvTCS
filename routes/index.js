var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendfile('www/index.html');
});

// router.get('/cutout', function(req, res, next) {
//   res.sendfile('www/cutout.html');
// });

router.get('/zoned', function(req, res, next) {
  res.sendfile('www/zoned.html');
});

router.get('/pumpout1', function(req, res, next) {
  res.sendfile('www/pumpout1.html');
});
router.get('/pumpout2', function(req, res, next) {
  res.sendfile('www/pumpout2.html');
});
router.get('/pumpout3', function(req, res, next) {
  res.sendfile('www/pumpout3.html');
});

module.exports = router;
