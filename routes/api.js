let express = require('express');
let mgdb = require('../utils/mgdb');
let finder = require('../utils/finder');
let pjson = require('../package.json');
let grid = require('../grid');
let calc = require('../utils/calc');
let router = express.Router();

router.get('/', function(req, res, next) {
	res.send("hi")
});
router.get('/carreg', function(req, res, next) {
	res.send([
		{ name: 'AGV01', ip: 43 },
		{ name: 'AGV02', ip: 44 }
	]);
});

router.get('/data/points/:id', function(req, res, next) {
	try {
		mgdb.findOne('gridtcs', 'maps', { _id: parseInt(req.params.id) }, (err, docs)=>{
			res.send({ 
				err: err, 
				points: docs.points 
			});
		});
	} catch (err) {
		res.send({ err });
	}
});

router.get('/data/maps', function(req, res, next) {
	try {
		mgdb.find('gridtcs', 'maps', { }, (err, doc)=>{
			res.send({ err, doc });
		});
	} catch (err) {
		res.send({ err });
	}
});

router.get('/data/maps/:id', function(req, res, next) {
	try {
		mgdb.findOne('gridtcs', 'maps', { _id: parseInt(req.params.id) }, (err, doc)=>{
			res.send({ err, doc });
		});
	} catch (err) {
		res.send({ err });
	}
});

router.get('/order/:x/:y/:no', function(req, res, next) {
	console.log('order');
	console.dir(req.body);
	let body = req.body;

	global.orders.push([ parseInt(req.params.x), parseInt(req.params.y), parseInt(req.params.no) ]);
    global.io.to('orders').emit('orders', global.orders);
	res.send(body);
});

router.get('/order', function(req, res, next) {
	res.send({ orders: global.orders, inprocess: global.inprocess });
});

router.get('/order/:mach', function(req, res, next) {
	console.log('order');
	let body = req.body;

	let o = global.orders.pimp.filter(d => d.mach == req.params.mach);
	let p = global.inprocess.pimp.filter(d => d.mach == req.params.mach);
	if(o.length + p.length > 0){
		res.send(o.length + p.length + '');
	}else{
		global.orders.pimp.push({ dt: new Date(), mach: req.params.mach });
		let o = global.orders.pimp.filter(d => d.mach == req.params.mach);
		console.dir(o);
		res.send(o.length+'');
	}
});

router.get('/orderpos/:mach', function(req, res, next) {
	console.log('order');
	let body = req.body;

	let o = global.orders.pimp.filter(d => d.mach == req.params.mach);
	let p = global.inprocess.pimp.filter(d => d.mach == req.params.mach);
	if(o.length + p.length > 0){
		res.send(global.orders.pimp.concat(global.inprocess.pimp));
	}else{
		global.orders.pimp.push({ dt: new Date(), mach: req.params.mach });
		res.send(global.orders.pimp.concat(global.inprocess.pimp));
	}
});

router.get('/clrorder', function(req, res, next) {
	if(global.inprocess.pimp.length > 0){
		global.inprocess.pimp = [];
	}
	res.send(200);
});

router.get('/clrorder/:mach', function(req, res, next) {
	if(global.inprocess.pimp.length > 0){
		global.inprocess.pimp = [];
	}
	res.send(200);
});
router.get('/clearorder/:sect', function(req, res, next) {
	global.orders[req.params.sect] = [];
	global.inprocess[req.params.sect] = [];
	res.send(200);
});
router.get('/clear/:sect', function(req, res, next) {
	global.inprocess[req.params.sect] = [];
	res.send(200);
});

router.get('/chkorder/:mach', function(req, res, next) {
	let o = global.orders.pimp.filter(d => d.mach == req.params.mach);
	let p = global.inprocess.pimp.filter(d => d.mach == req.params.mach);
	res.send(o.length + p.length + '');
});

// router.get('/chkorderpos/pimp', function(req, res, next) {
// 	let o = global.orders.pimp.concat(global.inprocess.pimp);
// 	res.send(o);
// });
router.get('/chkorderpos/:zone', function(req, res, next) {
	res.send(global.orders[req.params.zone].concat(global.inprocess[req.params.zone]));
});

router.get('/waitjob', function(req, res, next) {
	res.send({ number: global.waitjob.pimp });
});

router.get('/waitjob/:number', function(req, res, next) {
	global.waitjob.pimp = req.params.number;
	res.send(200);
});

router.get('/wait/:sect', function(req, res, next) {
	res.send({ number: global.waitjob[req.params.sect] });
});

router.get('/wait/:sect/:number', function(req, res, next) {
	global.waitjob[req.params.sect] = req.params.number;
	res.send(200);
});

router.post('/car/move', function(req, res, next) {
	let body = req.body;

	let inx = finder.CarIndex(global.cars, body.number);
    if(inx == -1){
      global.cars.push(body);
    }else{
    	grid.unblock(global.cars[inx].pos);
    	global.cars[inx] = body;
    }
    grid.block(body.pos);
    global.io.to('maps').emit('move', body);
	res.send(body);
});

router.post('/getroute', function(req, res, next) { 
	// console.log('getroute');
	// console.dir(req.body);
	let body = req.body;

	let path = grid.findPath(body.from, body.to, global.map.clone());
	if(path[0].length > -1){
		let p = calc.getPointRoute(path[0], global.doc.points, 3);
		// grid.blockMany(p[1]);
		
		// console.log(p[1]);
		res.send(p[1]);
	}else{
		console.log(null);
		res.send(null);
	}
});

router.post('/unblock', function(req, res, next) {
	// console.log('unblock');
	// console.dir(req.body);
	let body = req.body;
	if(body.length > 1){
		grid.unblock(body);
	}
	// console.log('unblock', body);
	
	res.send(body);
});


router.get('/zone/:zone', function(req, res, next) {
	// console.log(req.body);
	let z = req.params.zone;
	z = z.split(',');
	if(z.length == 3){
		global.zone[0] = (parseInt(z[0]) == -1 ? null : parseInt(z[0]) );
		global.zone[1] = (parseInt(z[1]) == -1 ? null : parseInt(z[1]) );
		global.zone[2] = (parseInt(z[2]) == -1 ? null : parseInt(z[2]) );
	}
	res.send(global.zone);
});

router.get('/zonenull/:zone', function(req, res, next) {
	global.zone[req.params.zone] = null;
	res.send(global.zone);
});
router.get('/zonenull', function(req, res, next) {
	global.zone = [null, null, null];
	res.send(global.zone);
});
router.get('/zone', function(req, res, next) {
	res.send(global.zone);
});

router.get('/order/pump/:mach', function(req, res, next) {
	console.log('order');
	let body = req.body;

	let o = global.orders.pump.filter(d => d.mach == req.params.mach);
	let p = global.inprocess.pump.filter(d => d.mach == req.params.mach);
	if(o.length + p.length > 0){
		res.send(o.length + p.length + '');
	}else{
		global.orders.pump.push({ dt: new Date(), mach: req.params.mach });
		let o = global.orders.pump.filter(d => d.mach == req.params.mach);
		console.dir(o);
		res.send(o.length+'');
	}
});

router.get('/clrorder/pump', function(req, res, next) {
	if(global.inprocess.pump.length > 0){
		global.inprocess.pump = [];
	}
	res.send(200);
});

router.get('/clrorder/pump/:mach', function(req, res, next) {
	if(global.inprocess.pump.length > 0){
		global.inprocess.pump = [];
	}
	res.send(200);
});

module.exports = router;
