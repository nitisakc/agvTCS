var colors = require('colors');

let cars = {
	reg: (obj)=>{
		let inx = global.cars.findIndex(d => d.number == parseInt(obj.number));
		if(inx == undefined || inx < 0){
			global.cars.push({
				number: parseInt(obj.number),
				lastupdate: new Date(),
				var: obj
			});
			console.log(`Add a new car number ${obj.number}.`);
		}
	},
	update: (obj)=>{
		let inx = global.cars.findIndex(d => d.number == parseInt(obj.number));
		if(inx == undefined || inx < 0){
			cars.reg(obj);
		}else{
			if(global.cars[inx]){
				global.cars[inx].var = obj;
				global.cars[inx].lastupdate = new Date();
			}
		}
	}
}

setInterval(()=>{
	let d = new Date();
	let n = global.cars.filter((el)=> { 
		return Math.abs(d - el.lastupdate) / 1000 < 3;
	});
	global.cars = n;
	if(global.io){ 
		global.io.emit('carsUpdate', global.cars);
		global.io.emit('time', d);
		global.io.emit('order', { orders: global.orders, inprocess: global.inprocess });
	 }
}, 1000);

module.exports = cars;