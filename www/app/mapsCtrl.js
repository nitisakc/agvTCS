let app = angular.module('APP', []);
app.controller('mainCtrl', function($scope, $http) {
	let svg = d3.select("svg"),
		gridSize = 10;

	let zoom = d3.zoom();

	let between = (x, min, max)=>{
		return x >= min && x <= max;
	};
	$scope.roundCompass15 = (int)=>{
		if(int <= 7){ return 270; }
		else if(between(int,8,22)){ return 285; }
		else if(between(int,23,37)){ return 300; }
		else if(between(int,38,52)){ return 315; }
		else if(between(int,53,67)){ return 330; }
		else if(between(int,68,82)){ return 345; }
		else if(between(int,83,97)){ return 0; }
		else if(between(int,98,112)){ return 15; }
		else if(between(int,113,127)){ return 30; }
		else if(between(int,128,142)){ return 45; }
		else if(between(int,143,157)){ return 60; }
		else if(between(int,158,172)){ return 75; }
		else if(between(int,173,187)){ return 90; }
		else if(between(int,188,202)){ return 105; }
		else if(between(int,203,217)){ return 120; }
		else if(between(int,218,232)){ return 135; }
		else if(between(int,233,247)){ return 150; }
		else if(between(int,248,262)){ return 165; }
		else if(between(int,263,277)){ return 180; }
		else if(between(int,278,292)){ return 195; }
		else if(between(int,293,307)){ return 210; }
		else if(between(int,308,322)){ return 225; }
		else if(between(int,323,337)){ return 240; }
		else if(between(int,338,352)){ return 255; }
		else if(int >= 352){ return 270; }
		else { console.log(int); return null; }
	};

	let zoomed =()=>{ 
		// console.log(d3.event.transform);
		gline.attr("transform", d3.event.transform);
		ggrid.attr("transform", d3.event.transform);
		gobj.attr("transform", d3.event.transform);
		glane.attr("transform", d3.event.transform);
		gpoint.attr("transform", d3.event.transform);
		garuco.attr("transform", d3.event.transform);
		gcctv.attr("transform", d3.event.transform);
		gcar.attr("transform", d3.event.transform);
		gtext.attr("transform", d3.event.transform);
	}
	let pad = (n, width, z)=>{
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	}
	$scope.pad = pad;

	let gline = svg.append("g")
					.attr('class','grid');
	let gobj = svg.append("g")
					.attr('class','obj');

	let gpad = svg.append("rect")
				    .attr("width", svg.attr("width"))
				    .attr("height", svg.attr("height"))
				    .style("fill", "none")
				    .style("pointer-events", "all")
				    .call(d3.zoom()
				        .scaleExtent([1 / 20, 6])
				        .on("zoom", zoomed));

	let glane = svg.append("g")
					.attr('class','lane');
	let ggrid = svg.append("g")
					.attr('class','ggrid');
	let gpoint = svg.append("g")
					.attr('class','point');
	let garuco = svg.append("g")
					.attr('class','aruco');
	let gcctv = svg.append("g")
					.attr('class','cctv');
	let gtext = svg.append("g")
					.attr('class','text');

	let gcar = svg.append("g")
					.attr('class','car');
	// var
	$scope.maps = {};
	$scope.cars = [];
	$scope.orders = [];
	$scope.newPoint = [];
	$scope.time = new Date();

	$scope.createGrid = (wh)=>{
		let girds = [];
		for(xi = 0; xi < wh[0]; xi++){ girds.push([xi * gridSize, 0, xi * gridSize, wh[1] * gridSize ]); }
		for(yi = 0; yi < wh[1]; yi++){ girds.push([0, yi * gridSize, wh[0] * gridSize, yi * gridSize ]); }

		gline.selectAll("line")
		    .data(girds)
		  .enter().append("line")
		    .attr("x1", d=> d[0])
		    .attr("y1", d=> d[1])
		    .attr("x2", d=> d[2])
		    .attr("y2", d=> d[3])
			.attr('stroke', '#919191')
			.attr('stroke-width', '0.4');

		girds = []; 
		for(xi = 0; xi < wh[0]; xi++){ 
			for(yi = 0; yi < wh[1]; yi++){ 
				girds.push([xi * gridSize,yi * gridSize]);
			}
		}

		var panZoom = svgPanZoom('#svg');
		panZoom.zoomBy(1.9);
		panZoom.pan({x: -300, y: -100})

		// svg.scale(.5);

		// zoom.scaleTo(svg, 0.5);

		// ggrid.selectAll("rect")
		//     .data(girds)
		//   .enter().append("rect")
		//   	.attr('class','on')
		//  	.attr("x", d=> d[0])
		// 	.attr("y", d=> d[1])
		// 	.attr("width", gridSize)
		// 	.attr("height", gridSize)
		// 	.attr("fill", "rgba(98, 226, 98, 1)")
		// 	.attr("opacity", 0.1)
		// 	.on("click", (d)=>{
		// 		let x = d[0]/gridSize;
		// 		let y = d[1]/gridSize;
		// 		var newid = prompt("Please enter AR number", "Add Point");
  // 				if (newid != null) {
  // 					$scope.maps.points.push({
  // 						no: parseInt(newid),
  // 						pos: [x, y],
  // 						status: 'g'
  // 					});

  // 					gpoint.append("image")
		// 			  	.attr('class','point')
		// 			  	.attr('no', parseInt(newid))
		// 			  	.attr('pos', [x, y])
		// 			  	.attr("xlink:href", "./img/point-g.png")
		// 			    .attr("x", (x * gridSize) - (gridSize / 2))
		// 				.attr("y", (y * gridSize) - (gridSize / 2))
		// 				.attr("width", gridSize * 2)
		// 				.attr("height", gridSize * 2);

		// 			$('#newPoint').val(JSON.stringify($scope.maps.points));
		// 		}
		// 	});
	}

	$scope.createObj = (obj)=>{
		gobj.selectAll("rect")
		    .data(obj)
		  .enter().append("rect")
		    .attr("x", d=> d[0] * gridSize)
			.attr("y", d=> d[1] * gridSize)
			.attr("width", d=> (d[2] - d[0]) * gridSize + gridSize)
			.attr("height", d=> (d[3] - d[1]) * gridSize + gridSize)
			.attr("fill", d=> "#2378ae");
	}

	$scope.createLane = (lane)=>{
		let lines = [];
		angular.forEach(lane, function(v, k) {
			let p1 = $scope.getPoint(v[0]);
			let p2 = $scope.getPoint(v[1]);
			lines.push([p1.pos[0], p1.pos[1], p2.pos[0], p2.pos[1]]);
		});
		glane.selectAll("line")
		    .data(lines)
		  .enter().append("line")
			.attr("x1", d=> (d[0] * gridSize) + (gridSize / 2))
			.attr("y1", d=> (d[1] * gridSize) + (gridSize / 2))
			.attr("x2", d=> (d[2] * gridSize) + (gridSize / 2))
			.attr("y2", d=> (d[3] * gridSize) + (gridSize / 2))
			.attr("stroke-width", 6)
			.attr("stroke", "rgba(98, 226, 98, 0.75)");
		// glane.selectAll("rect")
		//     .data(lane)
		//   .enter().append("rect")
		//     .attr("x", d=> d[0] * gridSize)
		// 	.attr("y", d=> d[1] * gridSize)
		// 	.attr("width", d=> (d[2] - d[0]) * gridSize + gridSize)
		// 	.attr("height", d=> (d[3] - d[1]) * gridSize + gridSize)
		// 	.attr("fill", "rgba(98, 226, 98, 0.6)");
	}

	$scope.createPoint = (point)=>{
		gpoint.selectAll("image")
		    .data(point)
		  .enter().append("image")
		  	.attr('class','point')
		  	.attr('no',d=> d.no)
		  	.attr('pos',d=> d.pos)
		  	.attr("xlink:href", d=> "./img/point-"+d.status+".png")
		    .attr("x", d=> (d.pos[0] * gridSize) - (gridSize / 2))
			.attr("y", d=> (d.pos[1] * gridSize) - (gridSize / 2))
			.attr("width", gridSize * 2)
			.attr("height", gridSize * 2)
			.on("click", (d)=>{
				// alert(JSON.stringify(d));
				// if(confirm(`ต้องการเรียกรถมาที่ตำแหน่ง ${d.mach} ใช้หรือไม่`)){
				// 	// $http.get(`/api/order/${d.mach}`, (res)=>{
				// 	// 	alert(res.data);
				// 	// });
				// }
			});
	}

	$scope.createAruco = (aruco)=>{
		garuco.selectAll("image")
		    .data(aruco)
		  .enter().append("image")
		  	.attr('class','aruco')
		  	.attr('no',d=> d.no)
		  	.attr('pos',d=> d.pos)
		  	.attr("xlink:href", d=> "./img/ar/" + d.no + ".png")
		    .attr("x", d=> (d.pos[0] * gridSize) - (gridSize / 2))
			.attr("y", d=> (d.pos[1] * gridSize) - (gridSize / 2))
			.attr("width", gridSize * 2)
			.attr("height", gridSize * 2)
			.attr("opacity", 0.4)
			.on("click", (d)=>{
				alert(JSON.stringify(d));
				if(confirm(`ต้องการเรียกรถมาที่ตำแหน่ง ${d.mach} ใช้หรือไม่`)){
					// $http.get(`/api/order/${d.mach}`, (res)=>{
					// 	alert(res.data);
					// });
				}
			})
		gtext.selectAll("text")
		    .data(aruco)
		  .enter().append("text")
		    .attr("x", d=> (d.pos[0] * gridSize) - (gridSize / 2))
			.attr("y", d=> (d.pos[1] * gridSize) - (gridSize / 2))
            .text( function (d) { return d.no; })
            .attr("font-family", "sans-serif")
            .attr("font-size", "6px")
            .attr("fill", "#000");
	}

	$scope.createCctv = (cctv)=>{
		gcctv.selectAll("image")
		    .data(cctv)
		  .enter().append("image")
		  	.attr('ip', d=> d.ip)
		  	.attr('class','cctv')
		  	.attr("xlink:href","./img/camera-icon.png")
		    .attr("x", d=> (d.pos[0] * gridSize) + (7))
			.attr("y", d=> (d.pos[1] * gridSize) + (7))
			.attr("width", 16)
			.attr("height", 16)
			.on("click", (d)=>{
				console.log(d);
				var c = gcctv.select('#rect'+d.no+'');
				var attr = c.attr("display");
				gcctv.selectAll("rect")
					.attr("display", 'none');
				if(attr == 'none'){ c.attr("display", ''); }
				else{ c.attr("display", 'none'); }
			});
		gcctv.selectAll("rect")
		    .data(cctv)
		  .enter().append("rect")
		  	.attr('id', d=> 'rect'+d.no)
		  	.attr('ip', d=> d.ip)
		  	.attr('class','zcctv')
		  	.attr("display", 'none')
		    .attr("x", d=> (d.area[0] * gridSize) - (2 * gridSize))
			.attr("y", d=> (d.area[1] * gridSize) - (2 * gridSize))
			.attr("width", d=> gridSize * (d.area[2] - d.area[0]))
			.attr("height", d=> gridSize * (d.area[3] - d.area[1]))
			.attr("fill", "RGBA(216,156,86,0.2)");
	}

	$scope.zoneClick = (z)=>{
		if($scope.zone[z]){
			let r = confirm(`Do you want to unlock zone ${z}?`);
			if(r){
				$scope.zone[z] = -1;
				$http.get(`/api/zone/${$scope.zone[0]},${$scope.zone[1]},${$scope.zone[2]}`).then((res)=>{
					if(res.status == 200){ $scope.zone = res.data; }
				});
			}
		}else{
			let r = prompt(`Input car number to lock zone ${z}`);
			if(r != null && r != ''){
				$scope.zone[z] = parseInt(r);
				$http.get(`/api/zone/${$scope.zone[0]},${$scope.zone[1]},${$scope.zone[2]}`).then((res)=>{
					if(res.status == 200){ $scope.zone = res.data; }
				});
			}
		}
	}

	$scope.car = {
		err: 7,
		getImg: (car)=>{
			return "./img/car/" + pad($scope.roundCompass15(car.deg),3) + '0' + (car.liftpos == 1 ? '1' : '0') + ".png"
		},
		create: (car)=>{
			// let c = $scope.cars.find(d => d.number == car.number);
			// if(!c){ $scope.cars.push(car); $scope.$apply(); }

			let k = 8;//$scope.car.err;
			let num = car.ar.length > 0 ? car.ar[0][0] : 0;
			let p = $scope.getPoint(num);
			gcar.append("image")
			  	.attr('class','car')
			  	.attr('cid', car.number)
			  	.attr('pos', num)
			  	.attr("xlink:href", "./img/car/" + pad($scope.roundCompass15(car.deg),3) + '0' + (car.liftpos == 1 ? '1' : '0') + ".png")
			  	// .attr("xlink:href", "./img/car/00001.png")
			    .attr("x", (p.pos[0] * gridSize) - ((k/2) * gridSize) + k - 2)
				.attr("y", (p.pos[1] * gridSize) - ((k/2) * gridSize) + k - 2)
				.attr("width", k * gridSize)
				.attr("height", k * gridSize)
				.on("click", ()=>{
				});

			gcar.append("text")
			  	.attr('class','carnumber')
			  	.attr('cid', car.number)
			    .attr("x", p.pos[0] * gridSize)
				.attr("y", p.pos[1] * gridSize + k)
	            .text(car.number)
	            .attr("font-family", "sans-serif")
	            .attr("font-size", "10px")
	            .attr("fill", "#000");
		},
		move: (car)=>{
			// let c = $scope.cars.findIndex(d => d.number == car.number);
			// if(c > -1){ $scope.cars[c] = car; $scope.$apply(); }

			var k = 8;//$scope.car.err;
			var sel = d3.selectAll('image.car[cid="'+car.number+'"').size();
			if(sel <= 0){
				$scope.car.create(car);
			}else{
				let num = car.ar.length > 0 && car.ar[0][1] < 60 ? car.ar[0][0] : 0;
				let p = $scope.maps.points.find(d => d.no == num);
				if(num != 0 && p){
					let p = $scope.getPoint(num);
					d3.select('image.car[cid="'+car.number+'"')
  						.attr("r", 90)
						.attr("x", (p.pos[0] * gridSize) - ((k/2) * gridSize) + k - 2)
						.attr("y", (p.pos[1] * gridSize) - ((k/2) * gridSize) + k - 2)
						// .attr("xlink:href", "./img/car/00001.png");
						.attr("xlink:href", "./img/car/" + pad($scope.roundCompass15(car.deg),3) + '0' + (car.liftpos == 1 ? '1' : '0') + ".png");

					d3.select('text.carnumber[cid="'+car.number+'"')
					    .attr("x", p.pos[0] * gridSize)
						.attr("y", p.pos[1] * gridSize + k);
				}
			}
		}
	}


	$scope.getPoint = (num)=>{
		let a = $scope.maps.points.find(d => d.no == num);
		if(a){
			return a;
		}else{
			return { no: 0, pos: [0,0] };
		}
	}

	setInterval(()=>{
		let n = $scope.cars.filter((el)=> { 
			return Math.abs($scope.time - (new Date(el.lastupdate))) / 1000 >= 5;
		});
		if(n){
			for(i = 0; i < n.length; i++){
				d3.selectAll('image.car[cid="'+n[i].number+'"').remove();
			}
		}
		n = $scope.cars.filter((el)=> { 
			return Math.abs($scope.time - (new Date(el.lastupdate))) / 1000 < 5;
		});
		$scope.cars = n;
	    $scope.$apply(); 
	}, 5000);

	$scope.socketInit = ()=>{
	    $scope.tcs = io.connect('/');
	    if($scope.tcs){
	        $scope.tcs.on('zone', function (msg) {
	        	$scope.zone = msg;
	        });
	        $scope.tcs.on('carsUpdate', function (data) {
	        	// d3.selectAll('image.car').remove();
	        	for(i = 0; i < data.length; i++){
	            	$scope.car.move(data[i].var);
	        		let c = $scope.cars.findIndex(d => d.number == data[i].number);
	        		if(c > -1){ 
	        			$scope.cars[c] = data[i]; 
	        		}else{
	        			$scope.cars.push(data[i]);
	        		}
	        		$scope.$apply(); 
	        	}
	        });
	        $scope.tcs.on('time', function (data) {
	        	$scope.time = new Date(data);
	        });

	        $scope.tcs.on('order', function (data) {
	        	console.log(data);
	        	$scope.orders = data;
	        	// $scope.orders.inprocess.push({mach: 'M2029', dt: new Date()});
	        	// $scope.orders.orders.push({mach: 'M2027', dt: new Date()});
	        	// $scope.orders.orders.push({mach: 'M2030', dt: new Date()});
	        });
	    }
	}

	$scope.sim = ()=>{
		let a = { number: 42, batt: 24.1, ar: [[51, 0, 0, 0]], deg: 0, liftpos: 2 };
		let b = { number: 41, batt: 24.1, ar: [[111, 0, 0, 0]], deg: 90, liftpos: 2 };

		let ea = [
			{ i: 0, n: 47, deg: 30, liftpos: 2 }, 
			{ i: 1, n: 62, deg: 0, liftpos: 2 }, 
			{ i: 2, n: 63, deg: 0, liftpos: 2 },
			{ i: 3, n: 64, deg: 0, liftpos: 2 }, 
			{ i: 4, n: 65, deg: 0, liftpos: 2 }, 
			{ i: 5, n: 66, deg: 0, liftpos: 2 },
			{ i: 6, n: 67, deg: 0, liftpos: 2 }, 
			{ i: 7, n: 67, deg: 90, liftpos: 2 }, 
			{ i: 8, n: 98, deg: 90, liftpos: 2 },
			{ i: 9, n: 98, deg: 90, liftpos: 1 }, 
			{ i: 11, n: 98, deg: 90, liftpos: 1 }, 
			{ i: 12, n: 67, deg: 90, liftpos: 1 },
			{ i: 13, n: 67, deg: 180, liftpos: 1 },
			{ i: 14, n: 66, deg: 180, liftpos: 1 },
			{ i: 15, n: 65, deg: 180, liftpos: 1 },
			{ i: 16, n: 64, deg: 180, liftpos: 1 },
			{ i: 17, n: 63, deg: 180, liftpos: 1 },
			{ i: 18, n: 62, deg: 180, liftpos: 1 },
			{ i: 19, n: 47, deg: 180, liftpos: 1 },
			{ i: 20, n: 41, deg: 180, liftpos: 1 },
			{ i: 21, n: 6, deg: 180, liftpos: 1 },
			{ i: 22, n: 40, deg: 180, liftpos: 1 },
			{ i: 23, n: 42, deg: 180, liftpos: 1 },
			{ i: 24, n: 39, deg: 180, liftpos: 1 },
			{ i: 25, n: 4, deg: 200, liftpos: 1 },
			{ i: 26, n: 49, deg: 220, liftpos: 1 },
			{ i: 27, n: 5, deg: 250, liftpos: 1 },
			{ i: 28, n: 45, deg: 200, liftpos: 1 },
			{ i: 29, n: 36, deg: 180, liftpos: 1 },
			{ i: 30, n: 43, deg: 180, liftpos: 1 },
			{ i: 31, n: 37, deg: 180, liftpos: 1 },
			{ i: 32, n: 21, deg: 180, liftpos: 1 },
			{ i: 33, n: 24, deg: 180, liftpos: 1 },
			{ i: 34, n: 35, deg: 180, liftpos: 1 },
			{ i: 35, n: 23, deg: 180, liftpos: 1 },
			{ i: 36, n: 12, deg: 180, liftpos: 1 },
			{ i: 37, n: 33, deg: 180, liftpos: 1 },
			{ i: 38, n: 25, deg: 180, liftpos: 1 },
			{ i: 39, n: 26, deg: 180, liftpos: 1 },
			{ i: 40, n: 38, deg: 180, liftpos: 1 },
			{ i: 41, n: 8, deg: 180, liftpos: 1 },
			{ i: 42, n: 13, deg: 180, liftpos: 1 },
			{ i: 43, n: 7, deg: 180, liftpos: 1 },
			{ i: 44, n: 7, deg: 180, liftpos: 2 },
			{ i: 45, n: 13, deg: 180, liftpos: 2 },
			{ i: 46, n: 8, deg: 180, liftpos: 2 },
			{ i: 47, n: 38, deg: 270, liftpos: 2 },
			{ i: 48, n: 25, deg: 300, liftpos: 2 },
			{ i: 49, n: 26, deg: 300, liftpos: 2 },
			{ i: 50, n: 33, deg: 0, liftpos: 2 },
			{ i: 51, n: 12, deg: 0, liftpos: 2 },
			{ i: 52, n: 23, deg: 0, liftpos: 2 },
			{ i: 53, n: 35, deg: 0, liftpos: 2 },
			{ i: 54, n: 24, deg: 0, liftpos: 2 },
			{ i: 55, n: 21, deg: 20, liftpos: 2 },
			{ i: 56, n: 37, deg: 0, liftpos: 2 },
			{ i: 57, n: 43, deg: 0, liftpos: 2 },
			{ i: 58, n: 36, deg: 0, liftpos: 2 },
			{ i: 59, n: 45, deg: 0, liftpos: 2 },
			{ i: 60, n: 5, deg: 70, liftpos: 2 },
			{ i: 61, n: 100, deg: 90, liftpos: 2 },
			{ i: 62, n: 107, deg: 90, liftpos: 2 },
			{ i: 63, n: 111, deg: 90, liftpos: 2 },
			{ i: 66, n: 111, deg: 0, liftpos: 2 },
			{ i: 67, n: 16, deg: 0, liftpos: 2 },
			{ i: 68, n: 39, deg: 340, liftpos: 2 },
			{ i: 69, n: 42, deg: 0, liftpos: 2 },
			{ i: 70, n: 40, deg: 0, liftpos: 2 },
			{ i: 71, n: 54, deg: 340, liftpos: 2 },
			{ i: 72, n: 50, deg: 0, liftpos: 2 },
			{ i: 73, n: 51, deg: 0, liftpos: 2 }

		];
		let eb = [
			{ i: 27, n: 111, deg: 0, liftpos: 2 },
			{ i: 28, n: 16, deg: 0, liftpos: 2 },
			{ i: 29, n: 39, deg: 340, liftpos: 2 },
			{ i: 30, n: 42, deg: 0, liftpos: 2 },
			{ i: 31, n: 40, deg: 0, liftpos: 2 },
			{ i: 32, n: 54, deg: 340, liftpos: 2 },
			{ i: 33, n: 50, deg: 0, liftpos: 2 },
			{ i: 34, n: 51, deg: 0, liftpos: 2 },
			{ i: 38, n: 50, deg: 0, liftpos: 2 },
			{ i: 39, n: 54, deg: 0, liftpos: 2 },
			{ i: 40, n: 54, deg: 90, liftpos: 2 },
			{ i: 41, n: 96, deg: 90, liftpos: 2 },
			{ i: 42, n: 96, deg: 90, liftpos: 1 },
			{ i: 43, n: 54, deg: 90, liftpos: 1 },
			{ i: 44, n: 54, deg: 180, liftpos: 1 },
			{ i: 45, n: 40, deg: 110, liftpos: 1 },
			{ i: 46, n: 31, deg: 180, liftpos: 1 },
			{ i: 47, n: 42, deg: 180, liftpos: 1 },
			{ i: 48, n: 32, deg: 200, liftpos: 1 },
			{ i: 49, n: 2, deg: 200, liftpos: 1 },
			{ i: 50, n: 20, deg: 180, liftpos: 1 },
			{ i: 64, n: 44, deg: 200, liftpos: 1 },
			{ i: 65, n: 49, deg: 220, liftpos: 1 },
			{ i: 66, n: 5, deg: 250, liftpos: 1 },
			{ i: 67, n: 45, deg: 200, liftpos: 1 },
			{ i: 68, n: 36, deg: 180, liftpos: 1 },
			{ i: 69, n: 43, deg: 180, liftpos: 1 },
			{ i: 70, n: 37, deg: 180, liftpos: 1 },
			{ i: 71, n: 21, deg: 180, liftpos: 1 },
			{ i: 72, n: 24, deg: 180, liftpos: 1 },
			{ i: 73, n: 35, deg: 180, liftpos: 1 },
			{ i: 74, n: 23, deg: 180, liftpos: 1 },
			{ i: 75, n: 12, deg: 180, liftpos: 1 },
			{ i: 76, n: 33, deg: 180, liftpos: 1 },
			{ i: 77, n: 25, deg: 180, liftpos: 1 },
			{ i: 78, n: 26, deg: 180, liftpos: 1 },
			{ i: 79, n: 38, deg: 180, liftpos: 1 },
			{ i: 80, n: 8, deg: 180, liftpos: 1 },
			{ i: 81, n: 13, deg: 180, liftpos: 1 },
			{ i: 82, n: 7, deg: 180, liftpos: 1 },
			{ i: 83, n: 7, deg: 180, liftpos: 2 },
			{ i: 84, n: 13, deg: 180, liftpos: 2 },
			{ i: 85, n: 8, deg: 180, liftpos: 2 },
			{ i: 86, n: 38, deg: 270, liftpos: 2 },
			{ i: 87, n: 26, deg: 300, liftpos: 2 },
			{ i: 88, n: 25, deg: 300, liftpos: 2 },
			{ i: 89, n: 33, deg: 0, liftpos: 2 },
			{ i: 90, n: 12, deg: 0, liftpos: 2 },
			{ i: 91, n: 23, deg: 0, liftpos: 2 },
			{ i: 92, n: 35, deg: 0, liftpos: 2 },
			{ i: 93, n: 24, deg: 0, liftpos: 2 },
			{ i: 94, n: 21, deg: 20, liftpos: 2 },
			{ i: 95, n: 37, deg: 0, liftpos: 2 },
			{ i: 96, n: 43, deg: 0, liftpos: 2 },
			{ i: 97, n: 36, deg: 0, liftpos: 2 },
			{ i: 98, n: 45, deg: 0, liftpos: 2 },
			{ i: 99, n: 5, deg: 70, liftpos: 2 },
			{ i: 100, n: 100, deg: 90, liftpos: 2 },
			{ i: 101, n: 107, deg: 90, liftpos: 2 },
			{ i: 102, n: 111, deg: 90, liftpos: 2 }
		];
	        
	    let i = 0;  
	    setTimeout(()=>{
		    $scope.car.move(a);
		    $scope.car.move(b);

		    setInterval(()=>{
				let aa = ea.find(d => d.i == i);
				if(aa){
					a.ar[0][0] = aa.n;
					a.deg = aa.deg;
					a.liftpos = aa.liftpos;
				}
				let bb = eb.find(d => d.i == i);
				if(bb){
					b.ar[0][0] = bb.n;
					b.deg = bb.deg;
					b.liftpos = bb.liftpos;
				}
			    $scope.car.move(a);
			    $scope.car.move(b);
		    	i++;
		    }, 500);
	    }, 1000);
	}

	$http.get('/api/data/maps/3').then((res)=>{
		if(res.data.err){ console.error(res.data.err); return; }
		$scope.maps = res.data.doc;

		$scope.createGrid($scope.maps.size);
		$scope.createObj($scope.maps.obj);
		$scope.createLane($scope.maps.lane);
		$scope.createPoint($scope.maps.points);
		$scope.createAruco($scope.maps.points);
		$scope.createCctv($scope.maps.cctv);

		// $scope.sim();
		$scope.socketInit();

		console.dir($scope.maps);
	}, (err)=>{
		console.error(err);
	});
});
