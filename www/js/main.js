$(document).ready(function() {
    if (!Raphael.svg) {
        window.location = './notsupported.html';
    }

    // suppress select events
    $(window).bind('selectstart', function(event) {
        event.preventDefault();
    });

    $('.header_title').click(function(){
        $('.accordion').toggleClass('hide');
        // $('.accordion').toggleClass('f');
        $('#algorithm_panel').toggleClass('op');
    });

    setTimeout(function(){
        $('.accordion').addClass('hide');
        // $('.accordion').addClass('f');
     },500);

    // initialize visualization
    Panel.init();
    Controller.init();
});
