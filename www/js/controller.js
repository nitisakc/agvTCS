/**
 * The visualization controller will works as a state machine.
 * See files under the `doc` folder for transition descriptions.
 * See https://github.com/jakesgordon/javascript-state-machine
 * for the document of the StateMachine module.
 */
var Controller = StateMachine.create({
    initial: 'none',
    events: [
        {
            name: 'init',
            from: 'none',
            to:   'ready'
        },
        {
            name: 'search',
            from: 'starting',
            to:   'searching'
        },
        {
            name: 'pause',
            from: 'searching',
            to:   'paused'
        },
        {
            name: 'finish',
            from: 'searching',
            to:   'finished'
        },
        {
            name: 'resume',
            from: 'paused',
            to:   'searching'
        },
        {
            name: 'cancel',
            from: 'paused',
            to:   'ready'
        },
        {
            name: 'modify',
            from: 'finished',
            to:   'modified'
        },
        {
            name: 'reset',
            from: '*',
            to:   'ready'
        },
        {
            name: 'clear',
            from: ['finished', 'modified'],
            to:   'ready'
        },
        {
            name: 'start',
            from: ['ready', 'modified', 'restarting'],
            to:   'starting'
        },
        {
            name: 'restart',
            from: ['searching', 'finished'],
            to:   'restarting'
        },
        {
            name: 'dragStart',
            from: ['ready', 'finished'],
            to:   'draggingStart'
        },
        {
            name: 'dragEnd',
            from: ['ready', 'finished'],
            to:   'draggingEnd'
        },
        {
            name: 'drawWall',
            from: ['ready', 'finished'],
            to:   'drawingWall'
        },
        {
            name: 'eraseWall',
            from: ['ready', 'finished'],
            to:   'erasingWall'
        },
        {
            name: 'rest',
            from: ['draggingStart', 'draggingEnd', 'drawingWall', 'erasingWall'],
            to  : 'ready'
        },
    ],
});

var plan = [{gpr:'W', type:'L',x1:0,y1:38,x2:33,y2:38},{gpr:'W', type:'L',x1:33,y1:38,x2:33,y2:7},{gpr:'W', type:'L',x1:33,y1:7,x2:130,y2:7},{gpr:'W', type:'L',x1:130,y1:7,x2:130,y2:0},{gpr:'W', type:'L',x1:130,y1:0,x2:198,y2:0},{gpr:'W', type:'L',x1:198,y1:0,x2:198,y2:68},{gpr:'W', type:'L',x1:0,y1:53,x2:33,y2:53},{gpr:'W', type:'L',x1:33,y1:53,x2:33,y2:128},{gpr:'W', type:'L',x1:33,y1:128,x2:54,y2:128},{gpr:'W', type:'L',x1:54,y1:128,x2:54,y2:136},{gpr:'W', type:'L',x1:54,y1:136,x2:150,y2:136},{gpr:'W', type:'L',x1:150,y1:136,x2:150,y2:128},{gpr:'W', type:'L',x1:150,y1:128,x2:179,y2:128},{gpr:'W', type:'L',x1:179,y1:128,x2:179,y2:83},{gpr:'W', type:'L',x1:179,y1:83,x2:198,y2:83},{gpr:'W', type:'P',x1:66,y1:67,x2:66,y2:67},{gpr:'W', type:'P',x1:99,y1:67,x2:99,y2:67},{gpr:'W', type:'P',x1:133,y1:67,x2:133,y2:67},{gpr:'W', type:'P',x1:166,y1:67,x2:166,y2:67},{gpr:'M', type:'B',x1:39,y1:76,x2:54,y2:126},{gpr:'M', type:'B',x1:66,y1:68,x2:81,y2:126},{gpr:'M', type:'B',x1:88,y1:76,x2:103,y2:122},{gpr:'M', type:'B',x1:111,y1:76,x2:126,y2:122},{gpr:'M', type:'B',x1:135,y1:76,x2:150,y2:126},{gpr:'M', type:'B',x1:157,y1:76,x2:172,y2:120},{gpr:'M', type:'B',x1:115,y1:16,x2:130,y2:40},{gpr:'M', type:'B',x1:145,y1:9,x2:160,y2:48},{gpr:'M', type:'B',x1:177,y1:9,x2:192,y2:62},{gpr:'M', type:'B',x1:45,y1:34,x2:55,y2:44},{gpr:'M', type:'B',x1:45,y1:21,x2:55,y2:31},{gpr:'M', type:'B',x1:45,y1:9,x2:55,y2:18},{gpr:'M', type:'B',x1:82,y1:34,x2:92,y2:44},{gpr:'M', type:'B',x1:82,y1:21,x2:92,y2:31},{gpr:'M', type:'B',x1:82,y1:9,x2:92,y2:18},{gpr:'M', type:'B',x1:95,y1:9,x2:105,y2:18},{gpr:'M', type:'B',x1:95,y1:21,x2:105,y2:31},{gpr:'M', type:'B',x1:95,y1:34,x2:105,y2:44},{gpr:'M', type:'B',x1:119,y1:58,x2:126,y2:66},{gpr:'W', type:'L',x1:199,y1:83,x2:259,y2:83},{gpr:'W', type:'L',x1:199,y1:57,x2:206,y2:57},{gpr:'W', type:'L',x1:206,y1:57,x2:206,y2:15},{gpr:'W', type:'L',x1:206,y1:15,x2:250,y2:15},{gpr:'W', type:'L',x1:250,y1:15,x2:250,y2:63},{gpr:'W', type:'L',x1:250,y1:63,x2:259,y2:63},{gpr:'W', type:'L',x1:259,y1:63,x2:259,y2:68},{gpr:'W', type:'P',x1:220,y1:20,x2:220,y2:20},{gpr:'W', type:'P',x1:220,y1:36,x2:220,y2:36},{gpr:'W', type:'P',x1:236,y1:36,x2:236,y2:36},{gpr:'W', type:'L',x1:260,y1:63,x2:275,y2:63},{gpr:'W', type:'L',x1:275,y1:63,x2:277,y2:63},{gpr:'W', type:'L',x1:277,y1:63,x2:277,y2:42},{gpr:'W', type:'L',x1:277,y1:42,x2:275,y2:42},{gpr:'W', type:'L',x1:275,y1:63,x2:275,y2:8},{gpr:'W', type:'L',x1:275,y1:8,x2:458,y2:8},{gpr:'W', type:'L',x1:458,y1:8,x2:458,y2:53},{gpr:'W', type:'L',x1:458,y1:49,x2:435,y2:49},{gpr:'W', type:'L',x1:435,y1:49,x2:435,y2:8},{gpr:'W', type:'L',x1:260,y1:83,x2:266,y2:83},{gpr:'W', type:'L',x1:266,y1:83,x2:266,y2:90},{gpr:'W', type:'L',x1:266,y1:90,x2:260,y2:90},{gpr:'W', type:'L',x1:260,y1:83,x2:260,y2:128},{gpr:'W', type:'L',x1:260,y1:128,x2:458,y2:128},{gpr:'W', type:'L',x1:458,y1:128,x2:458,y2:63},{gpr:'W', type:'L',x1:458,y1:63,x2:453,y2:63},{gpr:'W', type:'L',x1:453,y1:63,x2:453,y2:69},{gpr:'W', type:'L',x1:453,y1:69,x2:458,y2:69},{gpr:'M', type:'B',x1:283,y1:10,x2:311,y2:63},{gpr:'M', type:'B',x1:317,y1:10,x2:347,y2:63},{gpr:'M', type:'B',x1:351,y1:10,x2:380,y2:63},{gpr:'M', type:'B',x1:384,y1:10,x2:432,y2:63},{gpr:'M', type:'B',x1:274,y1:83,x2:291,y2:83},{gpr:'M', type:'B',x1:274,y1:99,x2:294,y2:108},{gpr:'M', type:'B',x1:274,y1:113,x2:313,y2:125},{gpr:'M', type:'B',x1:321,y1:78,x2:348,y2:100},{gpr:'M', type:'B',x1:321,y1:110,x2:348,y2:125},{gpr:'M', type:'B',x1:356,y1:78,x2:367,y2:128},{gpr:'M', type:'B',x1:375,y1:78,x2:390,y2:128},{gpr:'M', type:'B',x1:398,y1:78,x2:413,y2:128},{gpr:'M', type:'B',x1:421,y1:78,x2:432,y2:128},{gpr:'M', type:'B',x1:440,y1:78,x2:451,y2:128}];
var planSize = { x: 500, y: 137};
var block = [];

var addLine = function(d){
    if(d.x1 == d.x2){
        var dip = 0;
        if(d.y1 > d.y2){  
            dip = d.y1 - d.y2; dip++;
            for (i = 0; i < dip; i++) {
                block.push([d.x1, d.y2+i]);
            }
        }else{
            dip = d.y2 - d.y1; dip++;
            for (i = 0; i < dip; i++) {
                block.push([d.x1, d.y1+i]);
            }
        }
    }
    if(d.y1 == d.y2){
        var dip = 0;
        if(d.x1 > d.x2){  
            dip = d.x1 - d.x2; dip++;
            for (i = 0; i < dip; i++) {
                block.push([d.x2+i, d.y1]);
            }
        }else{
            dip = d.x2 - d.x1; dip++;
            for (i = 0; i < dip; i++) {
                block.push([d.x1+i, d.y1]);
            }
        }
    }
}

plan.forEach(function(d){
    if(d.type == 'L' || d.type == 'P'){
        addLine(d);
    }else if(d.type == 'B'){
        addLine({x1: d.x1, y1: d.y1, x2: d.x1, y2: d.y2});
        addLine({x1: d.x1, y1: d.y1, x2: d.x2, y2: d.y1});
        addLine({x1: d.x2, y1: d.y2, x2: d.x1, y2: d.y2});
        addLine({x1: d.x2, y1: d.y2, x2: d.x2, y2: d.y1});
    }
});

// var  block = [];
// var block = [
//     [175,75],[33,0],[33,1],[33,2],[33,3],[33,4],[33,5],[33,6],[33,7],[33,8],[33,10],[33,9],[33,11],[33,12],[33,13],[33,14],[33,15],[33,16],[33,17],[33,18],[33,19],[33,21],[33,20],[33,22],[33,23],[33,24],[33,25],[33,26],[33,27],[33,28],[33,29],[32,30],[33,30],[31,30],[28,30],[26,30],[23,30],[21,30],[20,30],[18,30],[17,30],[11,30],[10,30],[7,30],[4,30],[2,30],[1,30],[0,30],[3,30],[5,30],[6,30],[8,30],[9,30],[12,30],[14,30],[15,30],[13,30],[16,30],[19,30],[24,30],[25,30],[27,30],[30,30],[29,30],[22,30],[33,45],[0,45],[2,45],[3,45],[4,45],[1,45],[6,45],[8,45],[7,45],[5,45],[10,45],[9,45],[13,45],[12,45],[11,45],[14,45],[15,45],[17,45],[18,45],[19,45],[21,45],[23,45],[24,45],[26,45],[27,45],[28,45],[30,45],[31,45],[32,45],[16,45],[20,45],[22,45],[25,45],[29,45],[33,46],[33,47],[33,48],[33,49],[33,50],[33,51],[33,52],[33,53],[33,54],[33,55],[33,56],[33,57],[33,58],[33,59],[33,60],[33,61],[33,63],[33,64],[33,65],[33,62],[33,66],[33,67],[33,68],[33,69],[33,71],[33,72],[33,73],[33,70],[33,74],[33,75],[33,76],[33,78],[33,80],[33,81],[33,79],[33,77],[33,82],[33,83],[33,84],[33,85],[33,86],[33,87],[33,88],[33,89],[33,90],[33,91],[33,92],[33,94],[33,95],[33,93],[33,96],[33,97],[33,98],[33,99],[33,100],[33,101],[33,102],[33,103],[33,104],[33,105],[33,119],[33,117],[33,116],[33,118]
//     ,[33,115],[33,114],[33,113],[33,112],[33,111],[33,110],[33,109],[33,108],[33,107],[33,106],[176,75],[177,75],[178,75],[179,75],[180,75],[182,75],[183,75],[184,75],[185,75],[186,75],[187,75],[189,75],[191,75],[194,75],[195,75],[196,75],[197,75],[193,75],[192,75],[190,75],[188,75],[181,75],[175,76],[175,77],[175,78],[175,79],[175,80],[175,81],[175,83],[175,119],[175,118],[175,117],[175,116],[175,112],[175,111],[175,110],[175,109],[175,115],[175,114],[175,113],[175,108],[175,106],[175,105],[175,104],[175,107],[175,103],[175,102],[175,101],[175,100],[175,99],[175,98],[175,96],[175,95],[175,94],[175,97],[175,93],[175,92],[175,91],[175,90],[175,89],[175,88],[175,87],[175,86],[175,85],[175,82],[175,84],[166,60],[165,60],[165,61],[166,61],[131,60],[132,60],[132,61],[131,61],[198,61],[198,60],[198,59],[198,58],[198,57],[198,56],[198,55],[198,54],[198,53],[198,52],[198,51],[198,50],[198,49],[198,48],[198,47],[198,46],[198,45],[198,44],[198,43],[198,42],[198,41],[198,40],[198,39],[198,38],[198,37],[198,35],[198,34],[198,33],[198,32],[198,36],[198,31],[198,30],[198,29],[198,28],[198,27],[198,25],[198,24],[198,23],[198,22],[198,26],[198,21],[198,20],[198,19],[198,18],[198,17],[198,16],[198,15],[198,14],[198,13],[198,12],[198,11],[198,10],[198,9],[198,8],[198,6],[198,5],[198,4],[198,3],[198,2],[198,1],[198,0],[198,7],[198,75],[198,76],[198,77],[198,78],[198,79],[198,80],[198,81],[198,82],[198,83],[198,84],[198,85],[198,86],[198,87],[198,88],[198,89],[198,90],[198,91],[198,92],[198,93],[198,94],[198,95],[198,96],[198,98],[198,99],[198,100],[198,103],[198,104],[198,102],[198,101],[198,97],[198,105],[198,106],[198,107],[198,108],[198,109],[198,111],[198,112],[198,113],[198,114],[198,116],[198,117],[198,118],[198,119],[198,115],[198,110]
// ];

$.extend(Controller, {
    gridSize: [planSize.x, planSize.y], // number of nodes horizontally and vertically
    operationsPerSecond: 10,

    /**
     * Asynchronous transition from `none` state to `ready` state.
     */
    onleavenone: function() {
        var numCols = this.gridSize[0],
            numRows = this.gridSize[1];

        this.grid = new PF.Grid(numCols, numRows);

        View.init({
            numCols: numCols,
            numRows: numRows
        });
        View.generateGrid(function() {
            Controller.setDefaultStartEndPos();
            Controller.bindEvents();
            Controller.transition(); // transit to the next state (ready)
        });

        this.$buttons = $('.control_button');

        this.hookPathFinding();

        return StateMachine.ASYNC;
        // => ready
    },
    ondrawWall: function(event, from, to, gridX, gridY) {
        this.setWalkableAt(gridX, gridY, false);
        // => drawingWall
    },
    oneraseWall: function(event, from, to, gridX, gridY) {
        this.setWalkableAt(gridX, gridY, true);
        // => erasingWall
    },
    onsearch: function(event, from, to) {
        var grid,
            timeStart, timeEnd,
            finder = Panel.getFinder();

        timeStart = window.performance ? performance.now() : Date.now();
        grid = this.grid.clone();
        this.path = finder.findPath(
            this.startX, this.startY, this.endX, this.endY, grid
        );
        this.operationCount = this.operations.length;
        timeEnd = window.performance ? performance.now() : Date.now();
        this.timeSpent = (timeEnd - timeStart).toFixed(4);

        this.loop();
        // => searching
    },
    onrestart: function() {
        // When clearing the colorized nodes, there may be
        // nodes still animating, which is an asynchronous procedure.
        // Therefore, we have to defer the `abort` routine to make sure
        // that all the animations are done by the time we clear the colors.
        // The same reason applies for the `onreset` event handler.
        setTimeout(function() {
            Controller.clearOperations();
            Controller.clearFootprints();
            Controller.start();
        }, View.nodeColorizeEffect.duration * 1.2);
        // => restarting
    },
    onpause: function(event, from, to) {
        // => paused
    },
    onresume: function(event, from, to) {
        this.loop();
        // => searching
    },
    oncancel: function(event, from, to) {
        this.clearOperations();
        this.clearFootprints();
        // => ready
    },
    onfinish: function(event, from, to) {
        View.showStats({
            pathLength: PF.Util.pathLength(this.path),
            timeSpent:  this.timeSpent,
            operationCount: this.operationCount,
        });
        View.drawPath(this.path);
        // => finished
    },
    onclear: function(event, from, to) {
        this.clearOperations();
        this.clearFootprints();
        // => ready
    },
    onmodify: function(event, from, to) {
        // => modified
    },
    onreset: function(event, from, to) {
        setTimeout(function() {
            Controller.clearOperations();
            Controller.clearAll();
            Controller.buildNewGrid();
        }, View.nodeColorizeEffect.duration * 1.2);
        // => ready
    },

    /**
     * The following functions are called on entering states.
     */

    onready: function() {
        console.log('=> ready');
        this.setButtonStates({
            id: 1,
            text: 'Start Search',
            enabled: true,
            callback: $.proxy(this.start, this),
        }, {
            id: 2,
            text: 'Pause Search',
            enabled: false,
        }, {
            id: 3,
            text: 'Clear Walls',
            enabled: true,
            callback: $.proxy(this.reset, this),
        });
        // => [starting, draggingStart, draggingEnd, drawingStart, drawingEnd]
    },
    onstarting: function(event, from, to) {
        console.log('=> starting');
        // Clears any existing search progress
        this.clearFootprints();
        this.setButtonStates({
            id: 2,
            enabled: true,
        });
        this.search();
        // => searching
    },
    onsearching: function() {
        console.log('=> searching');
        this.setButtonStates({
            id: 1,
            text: 'Restart Search',
            enabled: true,
            callback: $.proxy(this.restart, this),
        }, {
            id: 2,
            text: 'Pause Search',
            enabled: true,
            callback: $.proxy(this.pause, this),
        });
        // => [paused, finished]
    },
    onpaused: function() {
        console.log('=> paused');
        this.setButtonStates({
            id: 1,
            text: 'Resume Search',
            enabled: true,
            callback: $.proxy(this.resume, this),
        }, {
            id: 2,
            text: 'Cancel Search',
            enabled: true,
            callback: $.proxy(this.cancel, this),
        });
        // => [searching, ready]
    },
    onfinished: function() {
        console.log('=> finished');
        this.setButtonStates({
            id: 1,
            text: 'Restart Search',
            enabled: true,
            callback: $.proxy(this.restart, this),
        }, {
            id: 2,
            text: 'Clear Path',
            enabled: true,
            callback: $.proxy(this.clear, this),
        });
    },
    onmodified: function() {
        console.log('=> modified');
        this.setButtonStates({
            id: 1,
            text: 'Start Search',
            enabled: true,
            callback: $.proxy(this.start, this),
        }, {
            id: 2,
            text: 'Clear Path',
            enabled: true,
            callback: $.proxy(this.clear, this),
        });
    },

    /**
     * Define setters and getters of PF.Node, then we can get the operations
     * of the pathfinding.
     */
    hookPathFinding: function() {

        PF.Node.prototype = {
            get opened() {
                return this._opened;
            },
            set opened(v) {
                this._opened = v;
                Controller.operations.push({
                    x: this.x,
                    y: this.y,
                    attr: 'opened',
                    value: v
                });
            },
            get closed() {
                return this._closed;
            },
            set closed(v) {
                this._closed = v;
                Controller.operations.push({
                    x: this.x,
                    y: this.y,
                    attr: 'closed',
                    value: v
                });
            },
            get tested() {
                return this._tested;
            },
            set tested(v) {
                this._tested = v;
                Controller.operations.push({
                    x: this.x,
                    y: this.y,
                    attr: 'tested',
                    value: v
                });
            },
        };

        this.operations = [];
    },
    bindEvents: function() {
        $('#draw_area').mousedown($.proxy(this.mousedown, this));
        $(window)
            .mousemove($.proxy(this.mousemove, this))
            .mouseup($.proxy(this.mouseup, this));
    },
    loop: function() {
        var interval = 1000 / this.operationsPerSecond;
        (function loop() {
            if (!Controller.is('searching')) {
                return;
            }
            Controller.step();
            setTimeout(loop, interval);
        })();
    },
    step: function() {
        var operations = this.operations,
            op, isSupported;

        do {
            if (!operations.length) {
                this.finish(); // transit to `finished` state
                return;
            }
            op = operations.shift();
            isSupported = View.supportedOperations.indexOf(op.attr) !== -1;
        } while (!isSupported);

        View.setAttributeAt(op.x, op.y, op.attr, op.value);
    },
    clearOperations: function() {
        this.operations = [];
    },
    clearFootprints: function() {
        View.clearFootprints();
        View.clearPath();
    },
    clearAll: function() {
        this.clearFootprints();
        View.clearBlockedNodes();
    },
    buildNewGrid: function() {
        this.grid = new PF.Grid(this.gridSize[0], this.gridSize[1]);
    },
    mousedown: function (event) {
        var coord = View.toGridCoordinate(event.pageX, event.pageY),
            gridX = coord[0],
            gridY = coord[1],
            grid  = this.grid;

        if (this.can('dragStart') && this.isStartPos(gridX, gridY)) {
            this.dragStart();
            return;
        }
        if (this.can('dragEnd') && this.isEndPos(gridX, gridY)) {
            this.dragEnd();
            return;
        }
        if (this.can('drawWall') && grid.isWalkableAt(gridX, gridY)) {
            this.drawWall(gridX, gridY);
            return;
        }
        if (this.can('eraseWall') && !grid.isWalkableAt(gridX, gridY)) {
            this.eraseWall(gridX, gridY);
        }
    },
    mousemove: function(event) {
        var coord = View.toGridCoordinate(event.pageX, event.pageY),
            grid = this.grid,
            gridX = coord[0],
            gridY = coord[1];

        if (this.isStartOrEndPos(gridX, gridY)) {
            return;
        }

        switch (this.current) {
        case 'draggingStart':
            if (grid.isWalkableAt(gridX, gridY)) {
                this.setStartPos(gridX, gridY);
            }
            break;
        case 'draggingEnd':
            if (grid.isWalkableAt(gridX, gridY)) {
                this.setEndPos(gridX, gridY);
            }
            break;
        case 'drawingWall':
            this.setWalkableAt(gridX, gridY, false);
            break;
        case 'erasingWall':
            this.setWalkableAt(gridX, gridY, true);
            break;
        }
    },
    mouseup: function(event) {
        if (Controller.can('rest')) {
            Controller.rest();
        }
    },
    setButtonStates: function() {
        $.each(arguments, function(i, opt) {
            var $button = Controller.$buttons.eq(opt.id - 1);
            if (opt.text) {
                $button.text(opt.text);
            }
            if (opt.callback) {
                $button
                    .unbind('click')
                    .click(opt.callback);
            }
            if (opt.enabled === undefined) {
                return;
            } else if (opt.enabled) {
                $button.removeAttr('disabled');
            } else {
                $button.attr({ disabled: 'disabled' });
            }
        });
    },
    /**
     * When initializing, this method will be called to set the positions
     * of start node and end node.
     * It will detect user's display size, and compute the best positions.
     */
    setDefaultStartEndPos: function() {
        var width, height,
            marginRight, availWidth,
            centerX, centerY,
            endX, endY,
            nodeSize = View.nodeSize;

        width  = $(window).width();
        height = $(window).height();

        marginRight = $('#algorithm_panel').width();
        availWidth = width - marginRight;

        centerX = Math.ceil(availWidth / 2 / nodeSize);
        centerY = Math.floor(height / 2 / nodeSize);

        this.setStartPos(centerX - 5, centerY);
        this.setEndPos(centerX + 5, centerY);

        var k = this;
        block.forEach(function(d) {
            k.setWalkableAt(d[0], d[1], false);
        });
    },
    setStartPos: function(gridX, gridY) {
        this.startX = gridX;
        this.startY = gridY;
        View.setStartPos(gridX, gridY);
    },
    setEndPos: function(gridX, gridY) {
        this.endX = gridX;
        this.endY = gridY;
        View.setEndPos(gridX, gridY);
    },
    setWalkableAt: function(gridX, gridY, walkable) {
        this.grid.setWalkableAt(gridX, gridY, walkable);
        View.setAttributeAt(gridX, gridY, 'walkable', walkable);

        // 
        // block.push([gridX, gridY]);
        // console.dir(block);
    },
    isStartPos: function(gridX, gridY) {
        return gridX === this.startX && gridY === this.startY;
    },
    isEndPos: function(gridX, gridY) {
        return gridX === this.endX && gridY === this.endY;
    },
    isStartOrEndPos: function(gridX, gridY) {
        return this.isStartPos(gridX, gridY) || this.isEndPos(gridX, gridY);
    },
});
