let app = angular.module('APP', []);
app.controller('mainCtrl', function ($scope, $http, $timeout) {
    $scope.convert = (b)=>{
        if(b) return "1";
        return "0";
    }
    $scope.initSelect2 = ()=>{
        $(".route").select2({
            tags: true,
            tokenSeparators: [',', ' '],
            width: '100%'
        });
        $('.select-single').select2({dropdownAutoWidth : true, width: 'auto'});
    }

    $scope.stepSel = '';
    $scope.confSel = '';
    $scope.carSel = null;

    $scope.genRoute = (r)=>{
        // console.log(r);
        alert($(`li[data-index="${r}"] .route`).select2('val'));
    }

    $scope.binddata = false;
    $scope.selStep = (s)=>{
        if(s == '') return;
        $scope.stepSel = s;
        $scope.confSel = '';
        $scope.binddata = true;
        $scope.events = $scope.station[s];

        $scope.events.forEach(el => {
            if(el.event == 'run'){
                let rs = [];
                el.route.forEach(r => {
                    if(typeof(r) == 'object'){
                        rs.push(r.num + 'z' + r.zone);
                    }else{
                        rs.push(r);
                    }
                })
                el.routeString = rs;
            }
        });

        console.log($scope.events);
        $timeout(()=>{
            $scope.initSelect2();
            feather.replace({ 'aria-hidden': 'true' });
            $scope.binddata = false;
            $('.select2.select2-container.select2-container--default.select2-container--focus').css('width','100%');
        }, 100);
    }

    $(document).ready(function() {
        $( "#sortable" ).sortable({});
    });

    $scope.routeLabel = (num)=>{
        if(typeof(num) === 'object'){
            return num.num + 'z' + num.zone;
        }else{
            return num;
        }
    }
    $scope.loadStep = (ip)=>{
        $http({
            method: 'GET',
            url: `http://${ip}/getconfig/steps`
        }).then(function successCallback(res) {
            console.log(res);
            $scope.station = res.data;
            $scope.stations = Object.keys($scope.station);
            $scope.selStep($scope.stepSel);
            // $scope.events = $scope.station[$scope.stations[0]];

            // $timeout(()=>{
            //     $scope.initSelect2();
            //     $( "#sortable" ).sortable({});
            //     feather.replace({ 'aria-hidden': 'true' });
            // }, 100);
        }, function errorCallback(res) {
            console.log(res);
        });
    }

    $scope.selConf = (c)=>{
        $scope.stepSel = '';
        $scope.confSel = c;
    }

    $scope.selCar = (car)=>{
        $scope.carSel = car;
        $scope.stepSel = '';
        $scope.confSel = '';
        $scope.loadStep(`192.168.108.${car.number}:3001`);
    }

    $http({
        method: 'GET',
        url: `http://192.168.108.7:3310/cars/data`
    }).then((res)=>{
        $scope.cars = res.data;
        console.log($scope.cars);
        $timeout(()=>{
            feather.replace({ 'aria-hidden': 'true' });
        }, 100);
    });

    $scope.save = ()=>{
        let list = [];//$("#sortable").sortable('toArray');
        let le = $('li.list-event');
        for(i = 0; i < le.length; i++){
            list.push($(le[i]).data('index'));
        }
        
        let objs = [];
        // console.log(list);
        list.forEach((l)=>{
            // console.log($scope.events[l]);
            let e = $scope.events[l];
            if(e.event == 'run'){
                console.log(e.routeString);
                // let r = $(`li[data-index="${l}"] .route`).select2('data').map((d)=>{ 
                if(typeof(e.routeString) == 'string'){
                    e.routeString = e.routeString.split(',');
                }
                let r = e.routeString.map((d)=>{ 
                    let text = d;
                    if(text.toString().includes('z')){
                        text = text.split('z');
                        return { num: parseInt(text[0]), zone: parseInt(text[1]) };
                    }else{
                        return parseInt(text);
                    }
                });
                let o = { event: e.event, dir: e.dir, route: r };
                if(e.osl){ o.osl = e.osl; }
                objs.push(o);
            }else if(e.event == 'turn'){
                objs.push({event: e.event, dir: e.dir, deg: parseInt(e.deg ? e.deg : 0) });
            }else if(e.event == 'lift'){
                objs.push({event: e.event, dir: parseInt(e.dir ? e.dir : 1) });
            }else{
                objs.push({event: e.event });
            }
        });

        console.log(objs);

        // $http({
        //     method: 'POST',
        //     url: `http://192.168.108.${$scope.carSel.number}:3001/savestep/${$scope.stepSel}`,
        //     headers: {
        //         'Content-Type': "application/json"
        //     },
        //     data: objs
        // }).then((res)=>{
        //     $scope.loadStep(`192.168.108.${$scope.carSel.number}:3001`);
        // });
    }
});