const cv = require('opencv4nodejs');
const request = require('request');
const schedule = require('node-schedule');
const fs = require('fs');

global.pallets = [
    { id: 211, url: 'http://192.168.101.211/capture', rect: [50, 170, 80, 70], count: 0, err: 0 }
];
let i = 0;
let readPallet = ()=>{
    let options = {
        url: global.pallets[i].url,
        method: "get",
        encoding: null,
        timeout: 8000
    };

    request(options, function (error, response, body) {
        if (error) {
            global.pallets[i].err++;
            if(global.pallets[i].err > 3){
                global.pallets[i].count = 0;
            }
            console.error('error:', error);
        } else {
            global.pallets[i].err = 0;
            fs.writeFileSync('camera/'+global.pallets[i].id+'.jpg', body);
            let camera = cv.imread('camera/'+global.pallets[i].id+'.jpg');
            let pallet = cv.imread('pallet/'+global.pallets[i].id+'.jpg');

            let rect = global.pallets[i].rect;
            camera = camera.getRegion(new cv.Rect(rect[0], rect[1], rect[2], rect[3])).bgrToGray();
            pallet = pallet.getRegion(new cv.Rect(rect[0], rect[1], rect[2], rect[3])).bgrToGray();

            cv.imwrite('diff/c'+global.pallets[i].id+'.jpg', camera);
            cv.imwrite('diff/p'+global.pallets[i].id+'.jpg', pallet);

            const spawn = require("child_process").spawn;
            const pythonProcess = spawn('python3',["py/pallet.py"]);

            pythonProcess.stdout.on('data', (data) => {
                console.log(global.pallets[i].id + ' ' +data.toString());
                let p = parseInt(data.toString());
                if(p > 85){
                    global.pallets[i].count++;
                }else{
                    global.pallets[i].count = 0;
                }
            });
        }

        i = i + 1;
        if(global.pallets.length == i){ i = 0; }
        setTimeout(()=>{
            readPallet();
        }, 5000);
    });
}

readPallet();