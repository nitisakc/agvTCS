let PF = require('pathfinding');

var calc = {};

calc.getBox = (x1, y1, x2, y2, margin = 0)=>{
	var d = [[x1,y1], [x1,y2], [x2,y1], [x2,y2]]
	var p = [x1+y1, x1+y2, x2+y1, x2+y2];
	let m = p.indexOf(Math.min(x1+y1, x1+y2, x2+y1, x2+y2));

	return [d[m][0] - margin, 
			d[m][1] - margin, 
			Math.abs(x1 - x2) + 1 + margin, 
			Math.abs(y1 - y2) + 1 + margin]; //[x, y, w, h]
}

calc.getInner = (x1, y1, x2, y2, margin = 0)=>{
	let d = calc.getBox(x1, y1, x2, y2, margin);
	// console.log(d);
	var res = [];
	for (var yi = d[1]; yi < (d[1]+d[3]); yi++) {
		for (var xi = d[0]; xi < (d[0]+d[2]); xi++) {
			res.push([xi, yi]);
		}
	}
	return res;
}


calc.getPointRoute = (a, p, len)=>{
	let output = [], point = [], count = 0;
	for (var i = 0; i < a.length; i++) {
		output.push(a[i]);
		let inx = p.findIndex(d => d.pos[0] == a[i][0] && d.pos[1] == a[i][1]);
		let red = p.findIndex(d => d.pos[0] == a[i][0] && d.pos[1] == a[i][1] && d.status == 'r');
		console.log(a[i], inx, red);
		if(inx > -1){ 
			if(red < 0){ count++; }
			point.push([a[i][0],a[i][1]]); 
		}
		if(count > len){ i = i + (a.length * 2); }
	}
	return [output,point];
}

// calc.getPointRoute = (a, p, len)=>{
// 	const po = p.filter(d => d.status != 'r').map(d => d.pos);
// 	let output = [], point = [], count = 0;
// 	for (var i = 0; i < a.length; i++) {
// 		output.push(a[i]);
// 		let inx = po.findIndex(d => d[0] == a[i][0] && d[1] == a[i][1]);
// 		if(inx > -1){ count++; point.push([a[i][0],a[i][1]]); }
// 		if(count > len){ i = i + (a.length * 2); }
// 	}

// 	return [output,point];
// }

calc.getRouteTime = (path)=>{
	var cpath = PF.Util.compressPath(path);
	let len = path.length * 0.5;
	len += (cpath.length - 2) * 5;
	return len;
}

module.exports = calc;