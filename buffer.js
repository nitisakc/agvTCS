const cv = require('opencv4nodejs');
const request = require('request');
const schedule = require('node-schedule');
const fs = require('fs');
let i = 0, last = '';
let readPallet = ()=>{

    request({ url: 'http://192.168.101.212/capture', method: "get", encoding: null, timeout: 3000 }, function (error, response, body) {
        if (error) {
            console.error('error: 212 capture', error);
            readPallet();
        } else {
            fs.writeFileSync('camera/212.jpg', body);

            request({ url: 'http://192.168.101.211/capture', method: "get", encoding: null, timeout: 3000 }, function (error, response, body) {
                if (error) {
                    console.error('error: 211 capture', error);
                    readPallet();
                } else {
                    fs.writeFileSync('camera/211.jpg', body);

                    const spawn = require("child_process").spawn;
                    const pythonProcess = spawn('python3',["py/bufferar.py" ]);

                    pythonProcess.stdout.on('data', (data) => {
                        let _data = data.toString().split(' ');
                        let s = 0;
                        // console.log('Raw Buffer ' + data.toString());
                        if(data.toString() == last){
                            i++;
                            if(i >= 1){
                                i = 0;
                                s = _data[0];
                            }
                        }else{ i = 0; }
                        request.get(
                            `http://192.168.101.43:3001/to/setbuf/${s}`,
                            (err, res, body)=>{
                                if(err){ console.error('error setbuf:', err);}
                                else{
                                    console.log('Send Buffer ' + data.toString());
                                }
                            }
                        );
                        last = data.toString();
                        // console.log(global.pallets[i].id + ' ' +data.toString());
                    });

                    setTimeout(()=>{
                        readPallet();
                    }, 3000);
                }
            });
        }
    });
}

readPallet();