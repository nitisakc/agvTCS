let PF = require('pathfinding');
let calc = require('./utils/calc');
let mgdb = require('./utils/mgdb');

let finder = new PF.AStarFinder();

let findPath = (f, t, grid)=>{
	var path = finder.findPath(f[0], f[1], t[0], t[1], grid);
	var cpath = PF.Util.compressPath(path);
	return [ path, cpath ];
}

let blockMany = (pos)=>{
	pos.forEach((p)=>{
		global.map.setWalkableAt(p[0], p[1], false);
	})
}
let unblockMany = (pos)=>{
	pos.forEach((p)=>{
		global.map.setWalkableAt(p[0], p[1], true);
	})
}

let block = (pos)=>{
	global.map.setWalkableAt(pos[0], pos[1], false);
}
let unblock = (pos)=>{
	global.map.setWalkableAt(pos[0], pos[1], true);
}

module.exports = {
	findPath: findPath,
	block: block,
	unblock: unblock,
	blockMany: blockMany,
	unblockMany: unblockMany
};

// module.exports = { 
// 	findPath: (f, t)=>{
// 		var path = finder.findPath(f[0], f[1], t[0], t[1], grid.clone());
// 		var cpath = PF.Util.compressPath(path);
// 		return [ path, cpath ];
// 	}
// };