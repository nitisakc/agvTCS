const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const index = require('./routes/index');
const maps = require('./routes/maps');
const cars = require('./routes/cars');
const orders = require('./routes/orders');
const api = require('./routes/api');
const buffer = require('./routes/buffer');
const reqmat = require('./routes/reqmat');
const config = require('./routes/config');

let app = express();

// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'www')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', index);
app.use('/maps', maps);
app.use('/cars', cars);
app.use('/orders', orders);
app.use('/api', api);
app.use('/buffer', buffer);
app.use('/reqmat', reqmat);
app.use('/config', config);

module.exports = app;
