let app = require('./app');
const http = require('http');
let PF = require('pathfinding');
let mgdb = require('./utils/mgdb');
let calc = require('./utils/calc');
var watch = require('array-watch');
let cars = require('./cars');
const request = require('request');

global.cars = [];
global.map = null;
global.clearMap = null;
global.doc = {};
global.lists = [];
global.zone = [null, null, null];
global.orders = { pimp: [], pump: [], wip2: [] };
global.inprocess = { pimp: [], pump: [], wip2: [] };
global.waitjob = { pimp: null, pump: null, wip2: null };
global.buffers = { pimp: [0,0,0,0], pump: [0,0,0,0], wip2: [0, 0, 0, 0, 0, 0, 0, 0], rackd: [0,0,0,0], lift: [0,0,0,0], other: [0,0,0,0] };
global.buffout = {
	wip:{
	  bool: [true,true,true,true,true,true,true],
	  jobs: [null,null,null,null,null,null,null]
	}
  }
global.buffer = {
	wipin: -1,
	pumpin: -1
}

// mgdb.find('gridtcs', 'reqmat', { rev: 0 }, (err, doc)=>{
//   	console.dir(doc[0].lists);
//   	global.lists = doc[0].lists
// });

const port = 3310;
app.set('port', port);
var server = http.createServer(app);
server.listen(port);
console.info('http://localhost:'+port+'/');

// let zone0 = [39, 42, 40, 54, 50, 51];
// let zone1 = [111];
// let zone2 = [5, 45, 36, 43, 37, 21, 24, 35];//, 23, 12, 33, 25, 26, 38, 8, 13, 7, 10, 14, 97, 69, 58, 57, 56];

let zone0 = [49, 44, 20, 4, 39, 42, 40, 54, 50, 51];
let zone1 = [317, 318];
let zone2 = [36,122, 139, 141, 137, 260, 141, 160, 169, 173, 74, 128, 120, 126, 133, 136, 261, 262, 263];


let startSocket = ()=>{
	global.io = require('socket.io').listen(server, { origins: '*:*'});
	global.io.on('connection', function(socket) {
		let query = socket.handshake.query;
		
		socket.join(query.room);
		global.io.to(socket.id).emit('conn', socket.id);
		socket.on('py', function(data) { global.io.emit('pys', 555); }); 
		
		socket.on('reqmat', function(data) {
			global.lists = data;
			mgdb.updateOne('gridtcs', 'reqmat', { rev: 0 }, { $set: { lists: data } }, (err)=>{
				console.log(err);
			});
			console.log('reqmat', data);
			global.io.emit('reqmat', data);
		});

		socket.on('var', function(data) {
			let car = data.number;
			cars.update(data);
			// console.log(data);

			if(data.ar.length > 0 && car != 45){//} && car == 41){
				data.ar.sort((a,b)=>{
					if (a[1] > b[1]) { return 1; }
				    if (b[1] > a[1]) { return -1; }
				    return 0;
				});

				let num = data.ar[0][1] < 50 ? data.ar[0][0] : null;
				let numw = data.ar[0][1] < 150 ? data.ar[0][0] : null;

				let z2 = zone2.find(d => d == num);
				if(z2){
					if(global.zone[0] == car){ global.zone[0] = null; }
					if(global.zone[1] == car){ global.zone[1] = null; }
					global.zone[2] = car;
				}
				let z1 = zone1.find(d => d == numw);
				if(z1){
					if(global.zone[0] == car){ global.zone[0] = null; }
					if(global.zone[2] == car){ global.zone[2] = null; }
					global.zone[1] = car;
				}
				let z0 = zone0.find(d => d == num);
				if(z0){
					if(global.zone[1] == car){ global.zone[1] = null; }
					if(global.zone[2] == car){ global.zone[2] = null; }
					global.zone[0] = car;
				}
			}
			global.io.emit('zone', global.zone);
			global.io.emit('buffwipin', global.buffer.wipin);
			global.io.emit('buffpumpin', global.buffer.pumpin);
			global.io.emit('buffrackd', global.buffer.rackd);
			global.io.emit('bufflift', global.buffer.lift);
			global.io.emit('buffother', global.buffer.other);
		});
	});
}

let pimp = {
	sentOrder: ()=>{
		setTimeout(()=>{
			if(global.orders.pimp.length > 0 && global.waitjob.pimp){
				// console.log("sentOrder");
				let m = global.orders.pimp[0].mach;
				request.get({
					method: 'GET',
					url: `http://192.168.108.${global.waitjob.pimp}:3001/to/set/${m}`,
					timeout: 2000
				}, (err, res, body)=>{
						if (err) { console.error('error: set order', err); }
						if(!err && res.statusCode == 200){
							global.orders.pimp[0].agv = global.waitjob.pimp;
							global.inprocess.pimp.push(global.orders.pimp[0]);
							mgdb.insert('gridtcs', 'orders', global.orders.pimp[0], ()=>{});
							global.orders.pimp.shift();
							console.log("Order shift");
						}
						pimp.sentOrder();
					}
				);
			}else{
				pimp.sentOrder();
			}
		}, 5000);
	}
}


let pump = {
	sentOrder: ()=>{
		setTimeout(()=>{
			if(global.orders.pump.length > 0 && global.waitjob.pump){
				// console.log("sentOrder");
				let m = global.orders.pump[0].mach;
				request.get({
					method: 'GET',
					url: `http://192.168.108.${global.waitjob.pump}:3001/to/set/${m}`,
					timeout: 2000
				}, (err, res, body)=>{
						if (err) { console.error('error: set order', err); }
						if(!err && res.statusCode == 200){
							global.orders.pump[0].agv = global.waitjob.pump;
							global.inprocess.pump.push(global.orders.pump[0]);
							mgdb.insert('gridtcs', 'orders', global.orders.pump[0], ()=>{});
							global.orders.pump.shift();
							console.log("Order shift");
						}
						pump.sentOrder();
					}
				);
			}else{
				pump.sentOrder();
			}
		}, 5000);
	}
}

// let pump = {
// 	sentOrder: ()=>{
// 		setTimeout(()=>{
// 			let w = global.buffout.wip, target = null;
// 			global.orders.pump = [];

// 			if(!w.bool[3] && w.jobs[3] == '1' && w.bool[4] && w.bool[5] && w.bool[6]){ target = 'WO3'; }
// 			else if(!w.bool[4] && w.jobs[4] == '1' && w.bool[5] && w.bool[6]){ target = 'WO4'; }
// 			else if(!w.bool[5] && w.jobs[5] == '1' && w.bool[6]){ target = 'WO5'; }
// 			else if(!w.bool[6] && w.jobs[6] == '1'){ target = 'WO6'; }

// 			console.log('target: ', target);

// 			if(target){
// 				let o = global.orders.pump.filter(d => d.mach == target);
// 				let p = global.inprocess.pump.filter(d => d.mach == target);

// 				console.log('o: ', o);
// 				console.log('p: ', p);

// 				if(o.length + p.length  == 0){ global.orders.pump.push({ dt: new Date(), mach: target }); }
// 			}

// 			console.log('order pump: ', global.orders.pump);

// 			if(global.orders.pump.length > 0 && global.waitjob.pump){
// 				console.log("sentOrder");
// 				let m = global.orders.pump[0].mach;
// 				request.get({
// 					method: 'GET',
// 					url: `http://192.168.101.${global.waitjob.pump}:3001/to/set/${m}`,
// 					timeout: 2000
// 				}, (err, res, body)=>{
// 						if (err) { console.error('error: set order', err); }
// 						if(!err && res.statusCode == 200){
// 							global.orders.pump[0].agv = global.waitjob.pump;
// 							global.inprocess.pump.push(global.orders.pump[0]);
// 							mgdb.insert('gridtcs', 'orders', global.orders.pump[0], ()=>{});
// 							global.orders.pump.shift();
// 							console.log("Order shift");
// 						}
// 						pump.sentOrder();
// 					}
// 				);
// 			}else{ pump.sentOrder(); }
// 		}, 5000);
// 	}
// }

startSocket();
pimp.sentOrder();
pump.sentOrder();
